#!/usr/bin/env python
#
# Copyright (c) 2018 Via Technology Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import unittest
import numpy as np
import pandas as pd
from numpy.testing import assert_almost_equal, assert_array_almost_equal
from via_sphere import Point3d, global_Point3d, Arc3d, distance_radians, \
    calculate_intersection, calculate_bisector, latitude, longitude

GOLDEN_ANGLE = np.arctan(2.0)
LATITUDE = 90.0 - np.rad2deg(GOLDEN_ANGLE)

LAT_LONG_ICOSAHEDRON = np.array([[90.0, 0.0],
                                 [LATITUDE, 180.0],
                                 [LATITUDE, -1.5 * 72.0],
                                 [LATITUDE, -0.5 * 72.0],
                                 [LATITUDE, 0.5 * 72.0],
                                 [LATITUDE, 1.5 * 72.0],
                                 [-LATITUDE, 2.0 * 72.0],
                                 [-LATITUDE, -2.0 * 72.0],
                                 [-LATITUDE, -1.0 * 72.0],
                                 [-LATITUDE, 0.0],
                                 [-LATITUDE, 1.0 * 72.0],
                                 [-90.0, 0.0]])
""" An Icosahedron with vertices at the North and South poles. """

PANDAS_ICOSAHEDRON = pd.DataFrame(LAT_LONG_ICOSAHEDRON, columns=['LAT', 'LON'])


class TestArc3d(unittest.TestCase):
    """ Arc3d class unit tests. """

    def test_ecefArc_init(self):
        """Test initialisation of Arc3d class."""
        points3d = global_Point3d(PANDAS_ICOSAHEDRON['LAT'],
                                  PANDAS_ICOSAHEDRON['LON'])

        for i in range(11):
            a = points3d[i]
            b = points3d[i + 1]
            arc = Arc3d(a, b)
            self.assertTrue(arc.a() == a)
            assert_almost_equal(abs(arc.pole()), 1.0)
            assert_almost_equal(GOLDEN_ANGLE, arc.length())
            assert_array_almost_equal(distance_radians(arc.b(), b), 0.0)

        # Test zero length arc
        a = points3d[5]
        arc = Arc3d(a, a)
        self.assertTrue(arc.a() == a)
        self.assertEqual(abs(arc.pole()), 0.0)
        self.assertEqual(arc.length(), 0.0)
        assert_array_almost_equal(distance_radians(arc.b(), a), 0.0)

        # Test antipodal points
        a = points3d[0]
        b = points3d[11]
        arc = Arc3d(a, b)
        self.assertTrue(arc.a() == a)
        self.assertEqual(abs(arc.pole()), 0.0)
        assert_almost_equal(arc.length(), np.pi)
        assert_array_almost_equal(distance_radians(arc.b(), b), 0.0)

    def test_repr(self):
        """Test Arc3d class python_repr function."""
        point_0 = global_Point3d(PANDAS_ICOSAHEDRON['LAT'][0],
                                 PANDAS_ICOSAHEDRON['LON'][0])
        point_1 = global_Point3d(PANDAS_ICOSAHEDRON['LAT'][1],
                                 PANDAS_ICOSAHEDRON['LON'][1])

        arc = Arc3d(point_0, point_1)
        self.assertEqual(repr(arc), 'Arc3d([[ 0.000000 0.000000 1.000000],'
                                    '[-0.000000 -1.000000 0.000000]],1.107149)')

    def test_str(self):
        """Test Arc3d class python str uses python_repr function."""
        points3d = global_Point3d(PANDAS_ICOSAHEDRON['LAT'],
                                  PANDAS_ICOSAHEDRON['LON'])
        point_0 = points3d[0]
        point_1 = points3d[1]
        arc = Arc3d(point_0, point_1)
        self.assertEqual(str(arc), 'Arc3d([[ 0.000000 0.000000 1.000000],'
                                   '[-0.000000 -1.000000 0.000000]],1.107149)')

    def test_eq(self):
        """Test Arcs in opposite directions."""
        points3d = global_Point3d(PANDAS_ICOSAHEDRON['LAT'],
                                  PANDAS_ICOSAHEDRON['LON'])
        point_0 = points3d[0]
        point_1 = points3d[1]
        a = Arc3d(point_0, point_1)
        b = Arc3d(point_1, point_0)

        self.assertTrue(a == a)
        self.assertTrue(b == b)
        self.assertFalse(a == b)
        self.assertFalse(b == a)

    def test_bool(self):
        """Test valid and invalid Arcs."""
        points3d = global_Point3d(PANDAS_ICOSAHEDRON['LAT'],
                                  PANDAS_ICOSAHEDRON['LON'])
        point_0 = points3d[0]
        point_1 = points3d[1]
        a = Arc3d(point_0, point_1)
        b = Arc3d(point_1, point_1)  # a null arc

        self.assertTrue(a)
        self.assertFalse(b)

    def test_position(self):
        """Test positions of points along an Arc on the Equator."""
        point_equator_1 = Point3d(1.0, 0.0, 0.0)
        point_equator_2 = Point3d(0.0, 1.0, 0.0)
        arc = Arc3d(point_equator_1, point_equator_2)

        self.assertTrue(arc.position(0.0) == point_equator_1)
        assert_array_almost_equal(distance_radians(arc.position(arc.length()),
                                                   point_equator_2), 0.0)

        recp_sqrt_2 = 1.0 / np.sqrt(2.0)
        point_equator_half = Point3d(recp_sqrt_2, recp_sqrt_2, 0.0)
        assert_array_almost_equal(distance_radians(arc.position(arc.length() / 2),
                                                   point_equator_half), 0.0)

        point_equator_3 = Point3d(0.0, -1.0, 0.0)
        assert_array_almost_equal(distance_radians(arc.position(-arc.length()),
                                                   point_equator_3), 0.0)

        point_equator_4 = Point3d(-1.0, 0.0, 0.0)
        assert_array_almost_equal(distance_radians(arc.position(2 * arc.length()),
                                                   point_equator_4), 0.0)

    def test_perp_position(self):
        """Test positions of points perpendicular to an Arc on the Equator."""
        point_equator_1 = Point3d(1.0, 0.0, 0.0)
        point_equator_2 = Point3d(0.0, 1.0, 0.0)
        arc = Arc3d(point_equator_1, point_equator_2)

        self.assertTrue(arc.perp_position(point_equator_1, 0.0) == point_equator_1)
        self.assertTrue(arc.perp_position(point_equator_2, 0.0) == point_equator_2)

        distance = np.deg2rad(1.0)  # 60 Nautical Miles
        perp_pos1 = arc.perp_position(point_equator_1, distance)
        assert_almost_equal(arc.along_track_distance(perp_pos1), 0.0)
        assert_almost_equal(arc.cross_track_distance(perp_pos1), distance)

        perp_pos2 = arc.perp_position(point_equator_1, -distance)
        assert_almost_equal(arc.along_track_distance(perp_pos2), 0.0)
        assert_almost_equal(arc.cross_track_distance(perp_pos2), -distance)

        perp_pos3 = arc.perp_position(point_equator_2, distance)
        assert_almost_equal(arc.along_track_distance(perp_pos3), arc.length())
        assert_almost_equal(arc.cross_track_distance(perp_pos3), distance)

        perp_pos4 = arc.perp_position(point_equator_2, -distance)
        assert_almost_equal(arc.along_track_distance(perp_pos4), arc.length())
        assert_almost_equal(arc.cross_track_distance(perp_pos4), -distance)

    def test_angle_position(self):
        """Test positions of points at an angle to an Arc on the Equator."""
        point_equator_1 = Point3d(1.0, 0.0, 0.0)
        point_equator_2 = global_Point3d(0.0, 1.0)  # 60 NM East
        arc = Arc3d(point_equator_1, point_equator_2)

        point_south = arc.angle_position(np.pi / 2)
        assert_almost_equal(latitude(point_south), -1.0)
        assert_almost_equal(longitude(point_south), 0.0)

        point_north = arc.angle_position(-np.pi / 2)
        assert_almost_equal(latitude(point_north), 1.0)
        assert_almost_equal(longitude(point_north), 0.0)

        point_south_east = arc.angle_position(np.pi / 4)
        assert_almost_equal(latitude(point_south_east), -0.70708884)
        assert_almost_equal(longitude(point_south_east), 0.70714268)

        point_north_east = arc.angle_position(-np.pi / 4)
        assert_almost_equal(latitude(point_north_east), 0.70708884)
        assert_almost_equal(longitude(point_north_east), 0.70714268)

    def test_cross_track_distance(self):
        """Test across track distances of points from an Arc."""
        points3d = global_Point3d(PANDAS_ICOSAHEDRON['LAT'],
                                  PANDAS_ICOSAHEDRON['LON'])

        point_0 = points3d[0]
        point_1 = points3d[1]
        arc = Arc3d(point_0, point_1)

        self.assertEqual(arc.cross_track_distance(point_0), 0.0)
        self.assertEqual(arc.cross_track_distance(point_1), 0.0)

        result2 = 0.5 * (np.pi - GOLDEN_ANGLE)
        assert_almost_equal(arc.cross_track_distance(points3d[2]), result2)

        result3 = 0.5 * GOLDEN_ANGLE
        assert_almost_equal(arc.cross_track_distance(points3d[3]), result3)
        assert_almost_equal(arc.cross_track_distance(points3d[4]), -result3)

        assert_almost_equal(arc.cross_track_distance(points3d[5]), -result2)

        assert_almost_equal(arc.cross_track_distance(points3d[6]), -result3)
        assert_almost_equal(arc.cross_track_distance(points3d[7]), result3)

        assert_almost_equal(arc.cross_track_distance(points3d[8]), result2)

        assert_almost_equal(arc.cross_track_distance(points3d[9]), 0.0)

        assert_almost_equal(arc.cross_track_distance(points3d[10]), -result2)

        assert_almost_equal(arc.cross_track_distance(points3d[11]), 0.0)

    def test_along_track_distance(self):
        """Test along track distances of points from an Arc."""
        points3d = global_Point3d(PANDAS_ICOSAHEDRON['LAT'],
                                  PANDAS_ICOSAHEDRON['LON'])

        point_0 = points3d[0]
        point_1 = points3d[1]
        arc = Arc3d(point_0, point_1)

        self.assertEqual(arc.along_track_distance(points3d[0]), 0.0)

        result1 = GOLDEN_ANGLE
        assert_almost_equal(arc.along_track_distance(points3d[1]), result1)

        result2 = 0.5 * GOLDEN_ANGLE
        assert_almost_equal(arc.along_track_distance(points3d[2]), result2)

        result3 = 0.5 * (GOLDEN_ANGLE - np.pi)
        assert_almost_equal(arc.along_track_distance(points3d[3]), result3)
        assert_almost_equal(arc.along_track_distance(points3d[4]), result3)

        assert_almost_equal(arc.along_track_distance(points3d[5]), result2)

        result4 = 2.124370686
        assert_almost_equal(arc.along_track_distance(points3d[6]), result4)
        assert_almost_equal(arc.along_track_distance(points3d[7]), result4)

        result5 = 0.5 * GOLDEN_ANGLE - np.pi
        assert_almost_equal(arc.along_track_distance(points3d[8]), result5)

        result6 = GOLDEN_ANGLE - np.pi
        assert_almost_equal(arc.along_track_distance(points3d[9]), result6)

        assert_almost_equal(arc.along_track_distance(points3d[10]), result5)

        assert_almost_equal(arc.along_track_distance(points3d[11]), np.pi)

        # Test points at poles.
        point_equator_1 = Point3d(1.0, 0.0, 0.0)
        point_equator_2 = Point3d(0.0, 1.0, 0.0)
        arc = Arc3d(point_equator_1, point_equator_2)

        north_pole = points3d[0]
        assert_almost_equal(arc.along_track_distance(north_pole), 0.0)

        south_pole = points3d[11]
        assert_almost_equal(arc.along_track_distance(south_pole), 0.0)

    def test_closest_distance(self):
        """Test closest distances of points from an Arc."""
        point_equator_1 = Point3d(1.0, 0.0, 0.0)
        point_equator_2 = Point3d(0.0, 1.0, 0.0)
        arc = Arc3d(point_equator_1, point_equator_2)

        assert_almost_equal(arc.closest_distance(point_equator_1), 0.0)
        assert_almost_equal(arc.closest_distance(point_equator_2), 0.0)

        points3d = global_Point3d(PANDAS_ICOSAHEDRON['LAT'],
                                  PANDAS_ICOSAHEDRON['LON'])

        assert_almost_equal(arc.closest_distance(points3d[4]),
                            0.463647609561)
        assert_almost_equal(arc.closest_distance(points3d[7]),
                            2.1243706866836156)

        north_pole = points3d[0]
        assert_almost_equal(arc.closest_distance(north_pole), np.pi / 2)

        south_pole = points3d[11]
        assert_almost_equal(arc.closest_distance(south_pole), np.pi / 2)

    def test_start_angle(self):
        """Test the angle of points from the start of an Arc."""
        points3d = global_Point3d(PANDAS_ICOSAHEDRON['LAT'],
                                  PANDAS_ICOSAHEDRON['LON'])

        # an arc dwon the IDL
        a = points3d[0]
        b = points3d[1]
        arc = Arc3d(a, b)

        # test zero angle to b
        angle = arc.start_angle(b)
        self.assertEqual(angle, 0.0)

        # test zero angle, along IDL from North Pole to South Pole
        angle = arc.start_angle(points3d[11])
        self.assertEqual(angle, 0.0)

        # test 180  degrees, along Greenwich meridan
        angle = arc.start_angle(points3d[9])
        assert_almost_equal(angle, np.pi)

        result = 0.4 * np.pi

        # test points by the IDL
        angle = arc.start_angle(points3d[2])
        assert_almost_equal(angle, -result)

        angle = arc.start_angle(points3d[5])
        assert_almost_equal(angle, result)

        # test points by the Greenwich meridian
        angle = arc.start_angle(points3d[3])
        assert_almost_equal(angle, -2 * result)

        angle = arc.start_angle(points3d[4])
        assert_almost_equal(angle, 2 * result)

    def test_turn_angle(self):
        """Test the turn angle of points from the end of an Arc."""
        points3d = global_Point3d(PANDAS_ICOSAHEDRON['LAT'],
                                  PANDAS_ICOSAHEDRON['LON'])

        a = points3d[0]
        b = points3d[1]
        arc = Arc3d(a, b)

        result_1 = 0.6 * np.pi
        result_2 = 0.2 * np.pi

        # test zero distance to b
        angle = arc.turn_angle(b)
        self.assertEqual(angle, 0.0)

        # test zero angle, along IDL from North Pole to South Pole
        c = points3d[11]
        angle = arc.turn_angle(c)
        self.assertEqual(angle, 0.0)

        for i in range(10):
            a = points3d[i]
            b = points3d[i + 1]
            arc = Arc3d(a, b)

            c = points3d[i + 2]

            angle = arc.turn_angle(c)

            if i == 0:
                assert_almost_equal(angle, -result_1)
            elif i == 9:
                assert_almost_equal(angle, result_1)
            elif i == 4 or i >= 6:
                assert_almost_equal(angle, result_2)
            else:  # elif i == 1:
                assert_almost_equal(angle, -result_2)

    def test_calculate_azimuth(self):
        """Test the azimuth of points along an Arc."""
        points3d = global_Point3d(PANDAS_ICOSAHEDRON['LAT'],
                                  PANDAS_ICOSAHEDRON['LON'])

        a = points3d[1]
        b = points3d[2]
        arc_1 = Arc3d(a, b)

        track_1a = arc_1.azimuth()
        assert_almost_equal(np.rad2deg(track_1a), 72.0)
        track_1b = arc_1.calculate_azimuth(arc_1.b())
        assert_almost_equal(np.rad2deg(track_1b), 108.0)

        arc_2 = Arc3d(b, a)

        track_2a = arc_2.azimuth()
        assert_almost_equal(np.rad2deg(track_2a), -72.0)
        track_2b = arc_2.calculate_azimuth(arc_2.b())
        assert_almost_equal(np.rad2deg(track_2b), -108.0)

        # North pole
        c = points3d[0]
        arc_np = Arc3d(c, a)

        track_np_a = arc_np.azimuth()
        self.assertEqual(track_np_a, np.pi)
        track_np_b = arc_np.calculate_azimuth(arc_np.b())
        self.assertEqual(track_np_b, np.pi)

        # South pole
        d = points3d[-1]
        arc_sp = Arc3d(d, a)

        track_sp_a = arc_sp.azimuth()
        self.assertEqual(track_sp_a, 0.0)
        track_sp_b = arc_sp.calculate_azimuth(arc_sp.b())
        self.assertEqual(track_sp_b, 0.0)

    def test_calculate_intersection(self):
        """Test the calculation of the intersection between two arcs."""
        points3d = global_Point3d(PANDAS_ICOSAHEDRON['LAT'],
                                  PANDAS_ICOSAHEDRON['LON'])

        # Same arcs, invalid intersection
        arc_1 = Arc3d(points3d[1], points3d[2])
        intersection0 = calculate_intersection(arc_1, arc_1)
        self.assertFalse(intersection0)

        # Intersecting arcs, valid intersection
        arc_2 = Arc3d(points3d[2], points3d[3])
        intersection1 = calculate_intersection(arc_1, arc_2)
        self.assertTrue(intersection1)
        assert_almost_equal(distance_radians(intersection1, points3d[2]), 0.0)

        # Opposing arcs, invalid intersection
        arc_3 = Arc3d(points3d[2], points3d[1])
        intersection2 = calculate_intersection(arc_1, arc_3)
        self.assertFalse(intersection2)

    def test_calculate_bisector(self):
        """Test the calculation of the bisector between two arcs."""
        points3d = global_Point3d(PANDAS_ICOSAHEDRON['LAT'],
                                  PANDAS_ICOSAHEDRON['LON'])

        arc_1 = Arc3d(points3d[1], points3d[2])
        arc_2 = Arc3d(points3d[2], points3d[3])

        arc = calculate_bisector(arc_1, arc_2)
        distance_1 = distance_radians(arc_1.pole(), arc.pole())
        distance_2 = distance_radians(arc_2.pole(), arc.pole())
        assert_almost_equal(distance_1, distance_2)


if __name__ == '__main__':
    unittest.main()
