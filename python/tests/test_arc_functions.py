#!/usr/bin/env python
#
# Copyright (c) 2018 Via Technology Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import unittest
import numpy as np
import pandas as pd
from numpy.testing import assert_almost_equal, assert_array_almost_equal
from via_sphere import EPSILON, Point3d, Arc3d, distance_radians, global_Point3d, \
    calculate_Arc3ds, calculate_turn_angles, calculate_xtds, calculate_atds, \
    calculate_positions, calculate_azimuths, calculate_closest_distances


GOLDEN_ANGLE = np.arctan(2.0)
LATITUDE = 90.0 - np.rad2deg(GOLDEN_ANGLE)

LAT_LONG_ICOSAHEDRON = np.array([[90.0, 0.0],
                                 [LATITUDE, 180.0],
                                 [LATITUDE, -1.5 * 72.0],
                                 [LATITUDE, -0.5 * 72.0],
                                 [LATITUDE, 0.5 * 72.0],
                                 [LATITUDE, 1.5 * 72.0],
                                 [-LATITUDE, 2.0 * 72.0],
                                 [-LATITUDE, -2.0 * 72.0],
                                 [-LATITUDE, -1.0 * 72.0],
                                 [-LATITUDE, 0.0],
                                 [-LATITUDE, 1.0 * 72.0],
                                 [-90.0, 0.0]])
""" An Icosahedron with vertices at the North and South poles. """

PANDAS_ICOSAHEDRON = pd.DataFrame(LAT_LONG_ICOSAHEDRON, columns=['LAT', 'LON'])


class TestArcFunctions(unittest.TestCase):

    def test_calculate_xtds(self):
        ecef_points = global_Point3d(PANDAS_ICOSAHEDRON['LAT'],
                                     PANDAS_ICOSAHEDRON['LON'])

        arc = Arc3d(ecef_points[0], ecef_points[1])
        xtds = calculate_xtds(arc, ecef_points)

        # North Pole
        self.assertEqual(xtds[0], 0.0)

        # The 5 verticies North of the Equator
        self.assertEqual(xtds[1], 0.0)

        result2 = 0.5 * (np.pi - np.arctan(2.0))
        assert_almost_equal(xtds[2], result2)

        result3 = 0.5 * np.arctan(2.0)
        assert_almost_equal(xtds[3], result3)
        assert_almost_equal(xtds[4], -result3)

        assert_almost_equal(xtds[5], -result2)

        # The 5 verticies South of the Equator
        assert_almost_equal(xtds[6], -result3)
        assert_almost_equal(xtds[7], result3)

        assert_almost_equal(xtds[8], result2)

        assert_almost_equal(xtds[9], 0.0)

        assert_almost_equal(xtds[10], -result2)

        # South Pole
        self.assertEqual(xtds[11], 0.0)

        xtds2 = calculate_xtds(arc, ecef_points[1: 10])
        assert_array_almost_equal(xtds2, xtds[1: 10])

    def test_calculate_atds(self):
        ecef_points = global_Point3d(PANDAS_ICOSAHEDRON['LAT'],
                                     PANDAS_ICOSAHEDRON['LON'])
        arc = Arc3d(ecef_points[0], ecef_points[1])
        atds = calculate_atds(arc, ecef_points)

        # North Pole
        self.assertEqual(atds[0], 0.0)

        # The 5 verticies North of the Equator
        result1 = np.arctan(2.0)
        assert_almost_equal(atds[1], result1)

        result2 = 0.5 * np.arctan(2.0)
        assert_almost_equal(atds[2], result2)

        result3 = 0.5 * (np.arctan(2.0) - np.pi)
        assert_almost_equal(atds[3], result3)
        assert_almost_equal(atds[4], result3)

        assert_almost_equal(atds[5], result2)

        # The 5 verticies South of the Equator
        result4 = 2.124370686
        assert_almost_equal(atds[6], result4)
        assert_almost_equal(atds[7], result4)

        result5 = 0.5 * np.arctan(2.0) - np.pi
        assert_almost_equal(atds[8], result5)

        result6 = np.arctan(2.0) - np.pi
        assert_almost_equal(atds[9], result6)

        assert_almost_equal(atds[10], result5)

        # South Pole
        self.assertEqual(atds[11], np.pi)

        atds2 = calculate_atds(arc, ecef_points[1: 10])
        assert_array_almost_equal(atds2, atds[1: 10])

    def test_calculate_Arc3ds(self):
        ecef_points = global_Point3d(PANDAS_ICOSAHEDRON['LAT'],
                                     PANDAS_ICOSAHEDRON['LON'])
        arcs3d = calculate_Arc3ds(ecef_points)
        self.assertEqual(len(arcs3d), len(ecef_points) - 1)

        lengths = np.array(arcs3d['length_'])

        for i, length in enumerate(lengths):
            assert_almost_equal(length, GOLDEN_ANGLE)

    def test_calculate_turn_angles(self):
        ecef_points = global_Point3d(PANDAS_ICOSAHEDRON['LAT'],
                                     PANDAS_ICOSAHEDRON['LON'])
        arcs3d = calculate_Arc3ds(ecef_points)
        turn_angles = calculate_turn_angles(arcs3d)

        self.assertEqual(len(turn_angles), len(ecef_points))
        self.assertEqual(turn_angles[0], 0.0)
        self.assertEqual(turn_angles[-1], 0.0)

        assert_almost_equal(turn_angles[1], -np.deg2rad(108.0))
        assert_almost_equal(turn_angles[-2], np.deg2rad(108.0))

        expected_result = np.deg2rad(36.0)
        for i in range(2, 5):
            assert_almost_equal(turn_angles[i], -expected_result)

        assert_almost_equal(turn_angles[5], expected_result)
        assert_almost_equal(turn_angles[6], -expected_result)

        for i in range(7, 10):
            assert_almost_equal(turn_angles[i], expected_result)

    def test_calculate_positions(self):
        ecef_points = global_Point3d(PANDAS_ICOSAHEDRON['LAT'],
                                     PANDAS_ICOSAHEDRON['LON'])
        arc = Arc3d(ecef_points[5], ecef_points[6])

        DISTANCE = GOLDEN_ANGLE / 10.0
        DISTANCES = np.array([0.0, DISTANCE,
                              2 * DISTANCE, 3 * DISTANCE,
                              4 * DISTANCE, 5 * DISTANCE,
                              6 * DISTANCE, 7 * DISTANCE,
                              8 * DISTANCE, 9 * DISTANCE])

        positions = calculate_positions(arc, DISTANCES)
        self.assertEqual(len(positions), len(DISTANCES))

        xtds = calculate_xtds(arc, positions)
        assert_array_almost_equal(xtds, np.zeros(len(DISTANCES), dtype=float))

        atds = calculate_atds(arc, positions)
        assert_array_almost_equal(atds, DISTANCES)

    def test_calculate_azimuths(self):
        ecef_points = global_Point3d(PANDAS_ICOSAHEDRON['LAT'],
                                     PANDAS_ICOSAHEDRON['LON'])
        arc = Arc3d(ecef_points[5], ecef_points[6])

        DISTANCE = GOLDEN_ANGLE / 10.0
        DISTANCES = np.array([0.0, DISTANCE,
                              2 * DISTANCE, 3 * DISTANCE,
                              4 * DISTANCE, 5 * DISTANCE,
                              6 * DISTANCE, 7 * DISTANCE,
                              8 * DISTANCE, 9 * DISTANCE])

        positions = calculate_positions(arc, DISTANCES)
        azimuths = calculate_azimuths(arc, positions)

        GROUND_TRACKS = np.array([2.51327412, 2.54167117,
                                  2.56257496, 2.5769034,
                                  2.58526799, 2.58801829,
                                  2.58526799, 2.5769034,
                                  2.56257496, 2.54167117])

        assert_array_almost_equal(azimuths, GROUND_TRACKS)

    def test_calculate_closest_distances(self):
        ecef_points = global_Point3d(PANDAS_ICOSAHEDRON['LAT'],
                                     PANDAS_ICOSAHEDRON['LON'])
        arcs3d = calculate_Arc3ds(ecef_points)

        distances = calculate_closest_distances(arcs3d, ecef_points[5])
        self.assertEqual(len(distances), len(arcs3d))

        CLOSEST_DISTANCES = np.array([1.01722197, 1.10714872,
                                      2.03444394, 1.10714872,
                                      0.0,        0.0,
                                      1.10714872, 2.03444394,
                                      2.03444394, 1.10714872,
                                      1.10714872])

        assert_array_almost_equal(distances, CLOSEST_DISTANCES)


if __name__ == '__main__':
    unittest.main()
