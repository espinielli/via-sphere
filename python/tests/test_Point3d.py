#!/usr/bin/env python
#
# Copyright (c) 2018 Via Technology Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import unittest
import numpy as np
from numpy.testing import assert_almost_equal, assert_array_almost_equal
from via_sphere import EPSILON, Point3d, distance_radians, \
    global_Point3d, latitude, longitude

GOLDEN_ANGLE = np.rad2deg(np.arctan(2.0))
LATITUDE = 90.0 - GOLDEN_ANGLE

LAT_LONG_ICOSAHEDRON = np.array([[90.0, 0.0],
                                 [LATITUDE, 180.0],
                                 [LATITUDE, -1.5 * 72.0],
                                 [LATITUDE, -0.5 * 72.0],
                                 [LATITUDE, 0.5 * 72.0],
                                 [LATITUDE, 1.5 * 72.0],
                                 [-LATITUDE, 2.0 * 72.0],
                                 [-LATITUDE, -2.0 * 72.0],
                                 [-LATITUDE, -1.0 * 72.0],
                                 [-LATITUDE, 0.0],
                                 [-LATITUDE, 1.0 * 72.0],
                                 [-90.0, 0.0]])
""" An Icosahedron with vertices at the North and South poles. """

ECEF_ICOSAHEDRON = np.array([[0.0, 0.0, 1.0],
                             [-0.8944271907493363, 0.0, 0.4472135960011169],
                             [-0.276393202421474, -0.8506508080328565, 0.4472135960011169],
                             [0.7236067972396153, -0.5257311123952776, 0.4472135960011169],
                             [0.7236067972396153, 0.5257311123952776, 0.4472135960011169],
                             [-0.276393202421474, 0.8506508080328565, 0.4472135960011169],
                             [-0.7236067972396153, 0.5257311123952776, -0.4472135960011169],
                             [-0.7236067972396153, -0.5257311123952776, -0.4472135960011169],
                             [0.276393202421474, -0.8506508080328565, -0.4472135960011169],
                             [0.8944271907493363, 0.0, -0.4472135960011169],
                             [0.276393202421474, 0.8506508080328565, -0.4472135960011169],
                             [0.0, 0.0, -1.0]])
""" The Icosahedron above in ECEF coordinates. """


class TestPoint3d(unittest.TestCase):

    def test_Point3d_lat_long_to_xyz(self):
        """Test conversion of Lat Longs to x, y, z coords using Icosahedron verticies."""
        for i in range(12):
            lat_long = LAT_LONG_ICOSAHEDRON[i]
            coords = global_Point3d(lat_long[0], lat_long[1])

            expected_result = ECEF_ICOSAHEDRON[i]
            assert_array_almost_equal(coords.p(), expected_result)

        # Test Latitude above North Pole
        lat_long = [91.0, 0.0]
        coords = global_Point3d(lat_long[0], lat_long[1])

        expected_result = ECEF_ICOSAHEDRON[0]
        assert_array_almost_equal(coords.p(), expected_result)

        # Test Latitude below South Pole
        lat_long = [-91.0, 0.0]
        coords = global_Point3d(lat_long[0], lat_long[1])

        expected_result = ECEF_ICOSAHEDRON[11]
        assert_array_almost_equal(coords.p(), expected_result)

    def test_distance_radians(self):
        ecef_point_0 = global_Point3d(ECEF_ICOSAHEDRON[0].tolist())
        self.assertEqual(distance_radians(ecef_point_0, ecef_point_0), 0.0)

        result_1 = np.arctan(2.0)
        for i in range(1, 6):
            ecef_point_i = global_Point3d(ECEF_ICOSAHEDRON[i].tolist())
            assert_almost_equal(distance_radians(ecef_point_0, ecef_point_i),
                                result_1)

        result_2 = np.pi - result_1  # 2.0344439363560154
        for i in range(6, 11):
            ecef_point_i = global_Point3d(ECEF_ICOSAHEDRON[i].tolist())
            assert_almost_equal(distance_radians(ecef_point_0, ecef_point_i),
                                result_2)

        ecef_point_11 = global_Point3d(ECEF_ICOSAHEDRON[11].tolist())
        assert_almost_equal(distance_radians(ecef_point_0, ecef_point_11),
                            np.pi)

    def test_Point3d_from_lat_long(self):
        """Test conversion of Lat Longs using Icosahedron verticies."""
        for i in range(12):
            lat_long = LAT_LONG_ICOSAHEDRON[i]
            ecef_point = global_Point3d(lat_long[0], lat_long[1])

            expected_result = ECEF_ICOSAHEDRON[i].tolist()
            assert_array_almost_equal(ecef_point.p(), expected_result)

        # Test Latitude above North Pole
        lat_long = [91.0, 0.0]
        ecef_point = global_Point3d(lat_long[0], lat_long[1])

        expected_result = ECEF_ICOSAHEDRON[0]
        assert_array_almost_equal(ecef_point.p(), expected_result)

        # Test Latitude below South Pole
        lat_long = [-91.0, 0.0]
        ecef_point = global_Point3d(lat_long[0], lat_long[1])

        expected_result = ECEF_ICOSAHEDRON[11]
        assert_array_almost_equal(ecef_point.p(), expected_result)

    def test_Point3d_to_lat_long(self):
        """Test conversion to Lat Longs using Icosahedron verticies."""
        for i in range(12):
            ecef_point = Point3d(ECEF_ICOSAHEDRON[i].tolist())
            lat_long = np.array([latitude(ecef_point),
                                 longitude(ecef_point)])

            expected_result = LAT_LONG_ICOSAHEDRON[i]
            assert_array_almost_equal(lat_long, expected_result)

    def test_repr(self):
        a = Point3d(ECEF_ICOSAHEDRON[1].tolist())
        self.assertEqual('Point3d([ -0.894427 0.000000 0.447214 ])',
                         repr(a))
        b = Point3d(ECEF_ICOSAHEDRON[10].tolist())
        self.assertEqual('Point3d([ 0.276393 0.850651 -0.447214 ])',
                         repr(b))

    def test_eq(self):
        a = Point3d(ECEF_ICOSAHEDRON[1].tolist())
        b = Point3d(ECEF_ICOSAHEDRON[10].tolist())

        self.assertTrue(a == a)
        self.assertTrue(b == b)
        self.assertFalse(a == b)
        self.assertFalse(b == a)

    def test_abs(self):
        for i in range(12):
            ecef_point = Point3d(ECEF_ICOSAHEDRON[i].tolist())
            assert_almost_equal(abs(ecef_point), 1.0)

    def test_bool(self):
        for i in range(12):
            ecef_point = Point3d(ECEF_ICOSAHEDRON[i].tolist())
            self.assertTrue(ecef_point)

    def test_neg(self):
        a = ECEF_ICOSAHEDRON[0].tolist()
        b = ECEF_ICOSAHEDRON[11].tolist()

        ecef_point = Point3d(a)
        ecef_point_anti = -ecef_point
        assert_array_almost_equal(ecef_point_anti.p(), b)

    def test_distance_radians(self):
        ecef_point_0 = Point3d(ECEF_ICOSAHEDRON[0].tolist())
        self.assertEqual(distance_radians(ecef_point_0, ecef_point_0), 0.0)

        result_1 = np.arctan(2.0)
        for i in range(1, 6):
            ecef_point_i = Point3d(ECEF_ICOSAHEDRON[i].tolist())
            assert_almost_equal(distance_radians(ecef_point_0, ecef_point_i),
                                result_1)

        result_2 = np.pi - result_1  # 2.0344439363560154
        for i in range(6, 11):
            ecef_point_i = Point3d(ECEF_ICOSAHEDRON[i].tolist())
            assert_almost_equal(distance_radians(ecef_point_0, ecef_point_i),
                                result_2)

        ecef_point_11 = Point3d(ECEF_ICOSAHEDRON[11].tolist())
        assert_almost_equal(distance_radians(ecef_point_0, ecef_point_11),
                            np.pi)


if __name__ == '__main__':
    unittest.main()
