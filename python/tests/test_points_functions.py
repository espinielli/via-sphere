#!/usr/bin/env python
#
# Copyright (c) 2018 Via Technology Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import unittest
import numpy as np
import pandas as pd
from numpy.testing import assert_almost_equal, assert_array_almost_equal
from via_sphere import EPSILON, Point3d, distance_radians, global_Point3d, \
    calculate_latitudes, calculate_longitudes, calculate_leg_lengths, \
    calculate_distances


GOLDEN_ANGLE = np.arctan(2.0)
LATITUDE = 90.0 - np.rad2deg(GOLDEN_ANGLE)

LAT_LONG_ICOSAHEDRON = np.array([[90.0, 0.0],
                                 [LATITUDE, 180.0],
                                 [LATITUDE, -1.5 * 72.0],
                                 [LATITUDE, -0.5 * 72.0],
                                 [LATITUDE, 0.5 * 72.0],
                                 [LATITUDE, 1.5 * 72.0],
                                 [-LATITUDE, 2.0 * 72.0],
                                 [-LATITUDE, -2.0 * 72.0],
                                 [-LATITUDE, -1.0 * 72.0],
                                 [-LATITUDE, 0.0],
                                 [-LATITUDE, 1.0 * 72.0],
                                 [-90.0, 0.0]])
""" An Icosahedron with vertices at the North and South poles. """

PANDAS_ICOSAHEDRON = pd.DataFrame(LAT_LONG_ICOSAHEDRON, columns=['LAT', 'LON'])

ECEF_ICOSAHEDRON = np.array([[0.0, 0.0, 1.0],
                             [-0.8944271907493363, 0.0, 0.4472135960011169],
                             [-0.276393202421474, -0.8506508080328565, 0.4472135960011169],
                             [0.7236067972396153, -0.5257311123952776, 0.4472135960011169],
                             [0.7236067972396153, 0.5257311123952776, 0.4472135960011169],
                             [-0.276393202421474, 0.8506508080328565, 0.4472135960011169],
                             [-0.7236067972396153, 0.5257311123952776, -0.4472135960011169],
                             [-0.7236067972396153, -0.5257311123952776, -0.4472135960011169],
                             [0.276393202421474, -0.8506508080328565, -0.4472135960011169],
                             [0.8944271907493363, 0.0, -0.4472135960011169],
                             [0.276393202421474, 0.8506508080328565, -0.4472135960011169],
                             [0.0, 0.0, -1.0]])
""" The Icosahedron above in ECEF coordinates. """


class TestPointsFunctions(unittest.TestCase):

    def test_global_Point3ds(self):
        'Test conversion of Icosahedron Lat Long verticies.'

        points3d = global_Point3d(PANDAS_ICOSAHEDRON['LAT'],
                                  PANDAS_ICOSAHEDRON['LON'])
        vectors3d = np.array(points3d['p_'])
        assert_array_almost_equal(vectors3d, ECEF_ICOSAHEDRON)

    def test_calculate_LatLongs(self):
        'Test conversion of Icosahedron Point3d verticies.'
        ecef_points = global_Point3d(PANDAS_ICOSAHEDRON['LAT'],
                                        PANDAS_ICOSAHEDRON['LON'])

        lats = calculate_latitudes(ecef_points)
        lons = calculate_longitudes(ecef_points)

        assert_array_almost_equal(lats, PANDAS_ICOSAHEDRON['LAT'])
        assert_array_almost_equal(lons, PANDAS_ICOSAHEDRON['LON'])

    def test_calculate_leg_lengths(self):
        'Test calculation of Icosahedron edge lengths.'
        ecef_points = global_Point3d(PANDAS_ICOSAHEDRON['LAT'],
                                        PANDAS_ICOSAHEDRON['LON'])
        leg_lengths = calculate_leg_lengths(ecef_points)
        self.assertEqual(leg_lengths[0], 0.0)

        expected_result = GOLDEN_ANGLE
        for i in range(1, 12):
            assert_array_almost_equal(leg_lengths[i], expected_result)

    def test_calculate_distances(self):
        'Test calculation of Icosahedron vertex distances from the North pole.'
        ecef_points = global_Point3d(PANDAS_ICOSAHEDRON['LAT'],
                                        PANDAS_ICOSAHEDRON['LON'])

        # Test North pole to South pole
        distances = calculate_distances(ecef_points, ecef_points[0])
        self.assertEqual(len(distances), 12)

        # North Pole
        self.assertEqual(distances[0], 0.0)

        # The 5 verticies North of the Equator
        result1 = GOLDEN_ANGLE
        for i in range(1, 6):
            assert_almost_equal(distances[i], result1)

        # The 5 verticies South of the Equator
        result2 = np.pi - GOLDEN_ANGLE
        for i in range(6, 11):
            assert_almost_equal(distances[i], result2)

        # South Pole
        self.assertEqual(distances[11], np.pi)


if __name__ == '__main__':
    unittest.main()
