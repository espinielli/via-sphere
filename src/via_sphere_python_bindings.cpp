//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Via Technology Ltd.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//////////////////////////////////////////////////////////////////////////////
/// @file via_sphere_python_bindings.cpp
/// @brief Contains the via::sphere python interface.
//////////////////////////////////////////////////////////////////////////////
#include <pybind11/numpy.h>
#include "via/sphere/Arc3d.hpp"
#include <pybind11/pybind11.h>
#include <pybind11/operators.h>
#include <pybind11/stl.h>

namespace py = pybind11;

namespace
{
  /// Calculate the latitudes of a numpy array of Point3ds.
  /// @param points the numpy array of Point3ds
  /// @return latitudes of the Point3ds in degrees, North positive.
  py::array_t<double> calculate_latitudes
    (py::array_t<via::sphere::Point3d> const& points)
  {
    py::array_t<double> values(points.size());

    auto p(points.unchecked<1>());
    auto l(values.mutable_unchecked<1>());
    for (auto i(0u); i < points.size(); ++i)
      l(i) = latitude(p(i));

    return values;
  }

  /// Calculate the longitudes of a numpy array of Point3ds.
  /// @param points the numpy array of Point3ds
  /// @return longitudes of the Point3ds in degrees, East positive.
  py::array_t<double> calculate_longitudes
    (py::array_t<via::sphere::Point3d> const& points)
  {
    py::array_t<double> values(points.size());

    auto p(points.unchecked<1>());
    auto l(values.mutable_unchecked<1>());
    for (auto i(0u); i < points.size(); ++i)
      l(i) = longitude(p(i));

    return values;
  }

  /// Calculate the Great Circle lengths of the legs between adjacent
  /// Point3ds.
  /// @param points the numpy array of Point3ds
  /// @return leg_lengths of the Point3ds in radians.
  py::array_t<double> calculate_leg_lengths
    (py::array_t<via::sphere::Point3d> const& points)
  {
    py::array_t<double> values(points.size());

    auto p(points.unchecked<1>());
    auto l(values.mutable_unchecked<1>());
    l(0) = 0.0;
    for (auto i(1u); i < points.size(); ++i)
      l(i) = distance_radians(p(i-1u), p(i));

    return values;
  }

  /// Calculate Great Circle distances between Point3ds and a Point3d.
  /// @param points the numpy array of Point3ds
  /// @param ref_point the reference Point3d to measure distance from.
  /// @return distances of the Point3ds from ref_point in radians.
  py::array_t<double> calculate_distances
    (py::array_t<via::sphere::Point3d> const& points,
     via::sphere::Point3d const& ref_point)
  {
    py::array_t<double> values(points.size());

    auto p(points.unchecked<1>());
    auto l(values.mutable_unchecked<1>());
    for (auto i(0u); i < points.size(); ++i)
      l(i) = distance_radians(p(i), ref_point);

    return values;
  }

  /// Convert a python list of Point3ds into a numpy array of Point3ds.
  /// @param points the python list of Point3ds, a vector in C++.
  /// @return the Point3ds in a numpy array.
  py::array_t<via::sphere::Point3d> to_array
    (std::vector<via::sphere::Point3d> const& points)
  {
    py::array_t<via::sphere::Point3d> numpy_points(points.size());
    auto p(numpy_points.mutable_unchecked<1>());
    for (auto i(0u); i < points.size(); ++i)
      p(i) = points[i];

    return numpy_points;
  }

  /// Calculate Great Circle across track distances between Point3ds and an
  /// Arc3d.
  /// @param arc the reference Arc3d to measure distance from.
  /// @param points the numpy array of Point3ds
  /// @return across track distances of the Point3ds from the Arc3d in
  /// radians.
  py::array_t<double> calculate_xtds
   (via::sphere::Arc3d const& arc,
    py::array_t<via::sphere::Point3d> const& points)
  {
    py::array_t<double> values(points.size());

    auto p(points.unchecked<1>());
    auto d(values.mutable_unchecked<1>());
    for (auto i(0u); i < points.size(); ++i)
      d(i) = arc.cross_track_distance(p(i));

    return values;
  }

  /// Calculate Great Circle along track distances of Point3ds along an
  /// Arc3d.
  /// @param arc the reference Arc3d to measure distance from.
  /// @param points the numpy array of Point3ds
  /// @return along track distances of the Point3ds along the Arc3d in
  /// radians.
  py::array_t<double> calculate_atds
    (via::sphere::Arc3d const& arc,
     py::array_t<via::sphere::Point3d> const& points)
  {
    py::array_t<double> values(points.size());

    auto p(points.unchecked<1>());
    auto d(values.mutable_unchecked<1>());
    for (auto i(0u); i < points.size(); ++i)
      d(i) = arc.along_track_distance(p(i));

    return values;
  }

  /// Calculate a numpy array of Arc3ds from a numpy array of Point3ds.
  /// @param points the numpy array of Point3ds
  /// @return the Arc3ds between the Point3ds.
  py::array_t<via::sphere::Arc3d> calculate_Arc3ds
    (py::array_t<via::sphere::Point3d> const& points)
  {
    py::array_t<via::sphere::Arc3d> arcs(points.size() -1);

    auto p(points.unchecked<1>());
    auto a(arcs.mutable_unchecked<1>());
    for (auto i(0u); i < points.size() -1; ++i)
      a(i) = via::sphere::Arc3d(p(i), p(i+1u));

    return arcs;
  }

  /// Calculate the turn angles between adjacent Arc3ds in radians.
  /// @param arcs the numpy array of Arc3ds
  /// @return turn angles between adjacent Arc3ds in radians,
  /// clockwise positive, anti-clockwise negative.
  py::array_t<double> calculate_turn_angles
    (py::array_t<via::sphere::Arc3d> const& arcs)
  {
    py::array_t<double> values(arcs.size() +1);

    auto a(arcs.unchecked<1>());
    auto d(values.mutable_unchecked<1>());
    d(0) = 0.0;
    d(arcs.size()) = 0.0;
    for (auto i(1u); i < arcs.size(); ++i)
      d(i) = a(i-1u).turn_angle(a(i).b());

    return values;
  }


  /// Calculate the Point3ds at distances along an Arc3d.
  /// @param arc the reference Arc3d to calculate positions along.
  /// @param distances the numpy array of distances along the arc in radians.
  /// @return the numpy array of Point3ds
  py::array_t<via::sphere::Point3d> calculate_positions
    (via::sphere::Arc3d const& arc, py::array_t<double> const& distances)
  {
    py::array_t<via::sphere::Point3d> points(distances.size());

    auto d(distances.unchecked<1>());
    auto p(points.mutable_unchecked<1>());
    for (auto i(0u); i < distances.size(); ++i)
      p(i) = arc.position(d(i));

    return points;
  }

  /// Calculate the azimuths of Point3ds along an Arc3d.
  /// @param arc the reference Arc3d to measure azimuth along.
  /// @param points the numpy array of Point3ds
  /// @return azimuths of the Point3ds along the arc in radians relative to
  /// True North.
  py::array_t<double> calculate_azimuths
    (via::sphere::Arc3d const& arc,
     py::array_t<via::sphere::Point3d> const& points)
  {
    py::array_t<double> values(points.size());

    auto p(points.unchecked<1>());
    auto d(values.mutable_unchecked<1>());
    for (auto i(0u); i < points.size(); ++i)
      d(i) = arc.calculate_azimuth(p(i));

    return values;
  }

  /// Calculate the closest distances of a vector of Arc3ds from a Point3d.
  /// @param arcs the numpy array of Arc3ds
  /// @param point the reference Point3d to measure distance from.
  /// @return closest distances of the Arc3ds from the Point3d in radians.
  py::array_t<double> calculate_closest_distances
    (py::array_t<via::sphere::Arc3d> const& arcs, via::sphere::Point3d const& point)
  {
    py::array_t<double> values(arcs.size());

    auto a(arcs.unchecked<1>());
    auto d(values.mutable_unchecked<1>());
    for (auto i(0u); i < arcs.size(); ++i)
      d(i) = a(i).closest_distance(point);

    return values;
  }
}

PYBIND11_MODULE(via_sphere, m)
{
  try { py::module::import("numpy"); }
  catch (...) { return; }

  // Python bindings for Vector3d functions
  m.def("dot", &via::sphere::dot, "3d vector dot product function.");
  m.def("cross", &via::sphere::cross, "3d vector cross product function.");
  m.def("norm", &via::sphere::norm, "The squared magnitude of a 3d vector.");
  m.def("length", &via::sphere::length, "The length of a 3d vector.");
  m.def("normalize", &via::sphere::normalize, "Normalize a 3d vector.");

  // Python bindings for Point3d constants
  m.attr("EPSILON") = via::sphere::EPSILON;
  m.attr("SQ_EPSILON") = via::sphere::SQ_EPSILON;
  m.attr("MIN_LENGTH") = via::sphere::MIN_LENGTH;
  m.attr("MIN_NORM") = via::sphere::MIN_NORM;

  // Python bindings for the Point3d class
  py::class_<via::sphere::Point3d>(m, "Point3d")
    .def(py::init<>())
    .def(py::init<via::sphere::Vector3d>())
    .def(py::init<double, double, double>())

    .def("p", &via::sphere::Point3d::p)
    .def("x", &via::sphere::Point3d::x)
    .def("y", &via::sphere::Point3d::y)
    .def("z", &via::sphere::Point3d::z)
    .def("__abs__", &via::sphere::Point3d::abs)
    .def("__bool__", &via::sphere::Point3d::is_valid)
    .def("__repr__", &via::sphere::Point3d::python_repr)

    .def(-py::self)
    .def(py::self == py::self)
    ;

  m.def("distance_radians", &via::sphere::distance_radians,
        "Calculate the Great Circle distance (in radians) between two Point3ds.");
  m.def("latitude", &via::sphere::latitude,
        "Calculate the latitude of a Point3d in degrees.");
  m.def("longitude", &via::sphere::longitude,
        "Calculate the longitude of a Point3d in degrees.");

  // Python numpy binding for the Point3d class
  PYBIND11_NUMPY_DTYPE(via::sphere::Point3d, p_);

  // Python bindings for numpy array of Point3d functions
  m.def("global_Point3d", py::vectorize(via::sphere::global_Point3d));
  m.def("calculate_latitudes", calculate_latitudes);
  m.def("calculate_longitudes", calculate_longitudes);
  m.def("calculate_leg_lengths", calculate_leg_lengths);
  m.def("calculate_distances", calculate_distances);
  m.def("to_array", to_array);

  // Python bindings for the Arc3d class
  py::class_<via::sphere::Arc3d>(m, "Arc3d")
    .def(py::init<>())
    .def(py::init<via::sphere::Point3d, via::sphere::Point3d>())
    .def("a", &via::sphere::Arc3d::a)
    .def("pole", &via::sphere::Arc3d::pole)
    .def("length", &via::sphere::Arc3d::length)
    .def("position", &via::sphere::Arc3d::position)
    .def("b", &via::sphere::Arc3d::b)
    .def("perp_position", &via::sphere::Arc3d::perp_position)
    .def("angle_position", &via::sphere::Arc3d::angle_position)
    .def("calculate_azimuth", &via::sphere::Arc3d::calculate_azimuth)
    .def("azimuth", &via::sphere::Arc3d::azimuth)
    .def("cross_track_distance", &via::sphere::Arc3d::cross_track_distance)
    .def("along_track_distance", &via::sphere::Arc3d::along_track_distance)
    .def("closest_distance", &via::sphere::Arc3d::closest_distance)
    .def("start_angle", &via::sphere::Arc3d::start_angle)
    .def("turn_angle", &via::sphere::Arc3d::turn_angle)
    .def("__bool__", &via::sphere::Arc3d::is_valid)
    .def("__repr__", &via::sphere::Arc3d::python_repr)

    .def(py::self == py::self)
    ;
  
  m.def("calculate_intersection", &via::sphere::calculate_intersection,
	    "Calculate the intersection between two Arc3ds.");
  m.def("calculate_bisector", &via::sphere::calculate_bisector,
        "Calculate the bisector arc between two Arc3ds.");

  // Python numpy binding for the Arc3d class
  PYBIND11_NUMPY_DTYPE(via::sphere::Arc3d, a_, pole_, length_);

  m.def("calculate_xtds", calculate_xtds);
  m.def("calculate_atds", calculate_atds);
  m.def("calculate_Arc3ds", calculate_Arc3ds);
  m.def("calculate_turn_angles", calculate_turn_angles);
  m.def("calculate_positions", calculate_positions);
  m.def("calculate_azimuths", calculate_azimuths);
  m.def("calculate_closest_distances", calculate_closest_distances);

#ifdef VERSION_INFO
  m.attr("__version__") = VERSION_INFO;
#else
  m.attr("__version__") = "dev";
#endif
}
