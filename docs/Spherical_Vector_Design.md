# Spherical Vector Design

The `via::sphere` library is based on `std::array`, see Figure 1

![via::sphere classes](images/Sphere_Classes.png)  
*Figure 1 via::sphere classes*

## Vector3d

`Vector3d` is a `std::array` of 3 doubles.

The [Vector3d.hpp](../include/via/sphere/Vector3d.hpp) file contains functions
and operators for performing vector operations on Vector3ds, i.e.:

- dot
- cross
- norm
- length
- addition
- subtraction
- scalar multiplication
- scalar division
- normalize

These functions correspond to built-in functions in 3D graphics libraries,
e.g.: OpenGL [cross](https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/cross.xhtml)
and [normalize](https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/normalize.xhtml).

## Point3d

A `Point3d` is a `Vector3d` representing a point on a unit sphere in
Earth Centred Earth Fixed (ECEF) coordinates,  
i.e.: a `Vector3d` where √(x² + y² + z²) == 1

In addition to the `Point3d` class, the [Point3d.hpp](../include/via/sphere/Point3d.hpp)
file contains the following functions:

- distance_radians: calculate the Great Circle distance between two Point3ds
- global_Point3d: create a Point3d from a latitude and longitude in degrees
- latitude: calculate the latitude of a Point3d in degrees
- longitude: calculate the longitude of a Point3d in degrees.

## Arc3d

An `Arc3d` represents the arc of Great Circle between two `Point3d`s on a
unit sphere.

The `Arc3d` class contains functions to calcuate:

- across track, along track and closest distances of a `Point3d`
- positions of `Point3d` along and perpendicular to the `Arc3d`
- the azimuth at positions along the `Arc3d`
- the turn angle to a `Point3d` from the end of the `Arc3d`.

See: [Arc3d.hpp](../include/via/sphere/Arc3d.hpp)

## point functions

The [point_functions.hpp](../include/via/sphere/point_functions.hpp) file
contains functions that process `std::vector`s of `Point3d`s.  
See Figure 2.

![via::sphere functions](images/Sphere_Classes_and_functions.png)  
*Figure 2 via::sphere functions*

## arc functions

The [arc_functions.hpp](../include/via/sphere/arc_functions.hpp) file
contains functions that process `std::vector`s of `Point3d`s relative to an `Arc3d`.  
See Figure 2.

## Python bindings

The [via_sphere_python_bindings.cpp](../src/via_sphere_python_bindings.cpp) file
contains the pyhton bindings for the C++ functions above.

The Python point and arc functions operate on numpy arrays of `Point3d`s.


