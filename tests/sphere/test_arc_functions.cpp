//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Via Technology Ltd.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//////////////////////////////////////////////////////////////////////////////
/// @file test_arc_functions.cpp
/// @brief Contains unit tests for arc_functions.
//////////////////////////////////////////////////////////////////////////////
#include "via/sphere/arc_functions.hpp"
#include "via/sphere/point_functions.hpp"
#include <boost/test/unit_test.hpp>
#include <limits>
#include <iostream>

using namespace via::sphere;

namespace
{
  // The following constants define an Icosahedron around the Earth.
  const auto GOLDEN_ANGLE(std::atan(2.0));
  const auto LATITUDE(90.0 - RAD_TO_DEG * GOLDEN_ANGLE);

  const std::vector<double> LATITUDES {90.0, LATITUDE, LATITUDE,
                                       LATITUDE, LATITUDE, LATITUDE,
                                       -LATITUDE, -LATITUDE, -LATITUDE,
                                       -LATITUDE, -LATITUDE, -90.0};

  const std::vector<double> LONGITUDES {0.0, 180.0, -1.5 * 72.0,
                                        -0.5 * 72.0, 0.5 * 72.0, 1.5 * 72.0,
                                        2.0 * 72.0, -2.0 * 72.0, -1.0 * 72.0,
                                        0.0, 1.0 * 72.0, 0.0};

  const auto CALCULATION_TOLERANCE(0.0000003); // 0.000 000 3% tolerance
}

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_SUITE(Test_point_functions)

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_calculate_Arc3ds)
{
  auto SIZE(LATITUDES.size());
  auto points3d(calculate_global_Point3ds(LATITUDES, LONGITUDES));
  BOOST_CHECK_EQUAL(SIZE, points3d.size());

  auto arcs3d(calculate_Arc3ds(points3d));
  BOOST_CHECK_EQUAL(SIZE -1, arcs3d.size());

  // Check that Arc lengths are close to the GOLDEN_ANGLE
  for (auto i(0u); i < arcs3d.size(); ++i)
    BOOST_CHECK_CLOSE(GOLDEN_ANGLE, arcs3d[i].length(), CALCULATION_TOLERANCE);
}
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_calculate_turn_angles)
{
  auto points3d(calculate_global_Point3ds(LATITUDES, LONGITUDES));
  auto arcs3d(calculate_Arc3ds(points3d));

  auto turn_angles(calculate_turn_angles(arcs3d));
  BOOST_CHECK_EQUAL(turn_angles.size(), points3d.size());
  BOOST_CHECK_EQUAL(0.0, turn_angles[0]);
  BOOST_CHECK_EQUAL(0.0, turn_angles[turn_angles.size() -1]);

  BOOST_CHECK_CLOSE(-108.0 * DEG_TO_RAD, turn_angles[1], CALCULATION_TOLERANCE);
  BOOST_CHECK_CLOSE(108.0 * DEG_TO_RAD, turn_angles[turn_angles.size() -2],
                    CALCULATION_TOLERANCE);

  auto expected_result(36.0 * DEG_TO_RAD);
  for (auto i(2u); i < 5u; ++i)
    BOOST_CHECK_CLOSE(-expected_result, turn_angles[i], CALCULATION_TOLERANCE);

  BOOST_CHECK_CLOSE(expected_result, turn_angles[5], CALCULATION_TOLERANCE);
  BOOST_CHECK_CLOSE(-expected_result, turn_angles[6], CALCULATION_TOLERANCE);

  for (auto i(7u); i < 10u; ++i)
    BOOST_CHECK_CLOSE(expected_result, turn_angles[i], CALCULATION_TOLERANCE);
}
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_calculate_xtds)
{
  auto points3d(calculate_global_Point3ds(LATITUDES, LONGITUDES));
  Arc3d arc(points3d[0], points3d[1]);

  auto xtds(calculate_xtds(arc, points3d));
  BOOST_CHECK_EQUAL(xtds.size(), points3d.size());

  auto index(0u);

  // The North Pole
  BOOST_CHECK_SMALL(xtds[index++], CALCULATION_TOLERANCE);

  // The 5 verticies North of the Equator
  BOOST_CHECK_SMALL(xtds[index++], CALCULATION_TOLERANCE);

  auto result2(0.5 * (M_PI - GOLDEN_ANGLE));
  BOOST_CHECK_CLOSE(result2, xtds[index++], CALCULATION_TOLERANCE);

  auto result3(0.5 * GOLDEN_ANGLE);
  BOOST_CHECK_CLOSE(result3, xtds[index++], CALCULATION_TOLERANCE);
  BOOST_CHECK_CLOSE(-result3, xtds[index++], CALCULATION_TOLERANCE);

  BOOST_CHECK_CLOSE(-result2, xtds[index++], CALCULATION_TOLERANCE);

  // The 5 verticies South of the Equator
  BOOST_CHECK_CLOSE(-result3, xtds[index++], CALCULATION_TOLERANCE);
  BOOST_CHECK_CLOSE(result3, xtds[index++], CALCULATION_TOLERANCE);

  BOOST_CHECK_CLOSE(result2, xtds[index++], CALCULATION_TOLERANCE);

  BOOST_CHECK_SMALL(xtds[index++], CALCULATION_TOLERANCE);

  BOOST_CHECK_CLOSE(-result2, xtds[index++], CALCULATION_TOLERANCE);

  // The South Pole
  BOOST_CHECK_SMALL(xtds[index], CALCULATION_TOLERANCE);
}
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_calculate_atds)
{
  auto points3d(calculate_global_Point3ds(LATITUDES, LONGITUDES));
  Arc3d arc(points3d[0], points3d[1]);

  auto atds(calculate_atds(arc, points3d));
  BOOST_CHECK_EQUAL(atds.size(), points3d.size());

  auto index(0u);

  // The North Pole
  BOOST_CHECK_SMALL(atds[index++], CALCULATION_TOLERANCE);

  // The 5 verticies North of the Equator
  auto result1(GOLDEN_ANGLE);
  BOOST_CHECK_CLOSE(result1, atds[index++], CALCULATION_TOLERANCE);

  auto result2(0.5 * GOLDEN_ANGLE);
  BOOST_CHECK_CLOSE(result2, atds[index++], CALCULATION_TOLERANCE);

  auto result3(0.5 * (GOLDEN_ANGLE - M_PI));
  BOOST_CHECK_CLOSE(result3, atds[index++], CALCULATION_TOLERANCE);
  BOOST_CHECK_CLOSE(result3, atds[index++], CALCULATION_TOLERANCE);

  BOOST_CHECK_CLOSE(result2, atds[index++], CALCULATION_TOLERANCE);

  // The 5 verticies South of the Equator
  auto result4(2.124370686);
  BOOST_CHECK_CLOSE(result4, atds[index++], CALCULATION_TOLERANCE);
  BOOST_CHECK_CLOSE(result4, atds[index++], CALCULATION_TOLERANCE);

  auto result5(0.5 * GOLDEN_ANGLE - M_PI);
  BOOST_CHECK_CLOSE(result5, atds[index++], CALCULATION_TOLERANCE);

  auto result6(GOLDEN_ANGLE - M_PI);
  BOOST_CHECK_CLOSE(result6, atds[index++], CALCULATION_TOLERANCE);

  BOOST_CHECK_CLOSE(result5, atds[index++], CALCULATION_TOLERANCE);

  // The South Pole
  BOOST_CHECK_CLOSE(M_PI, atds[index], CALCULATION_TOLERANCE);
}
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_calculate_positions)
{
  auto points3d(calculate_global_Point3ds(LATITUDES, LONGITUDES));
  Arc3d arc(points3d[5], points3d[6]);

  auto DISTANCE(GOLDEN_ANGLE / 10.0);
  std::vector<double> distances{ 0.0, DISTANCE,
                                 2 * DISTANCE, 3 * DISTANCE,
                                 4 * DISTANCE, 5 * DISTANCE,
                                 6 * DISTANCE, 7 * DISTANCE,
                                 8 * DISTANCE, 9 * DISTANCE };

  auto positions(calculate_positions(arc, distances));
  BOOST_CHECK_EQUAL(positions.size(), distances.size());

  BOOST_CHECK(points3d[5] == positions[0]);

  auto xtds(calculate_xtds(arc, positions));
  auto atds(calculate_atds(arc, positions));
  for (auto i(0u); i < distances.size(); ++i)
  {
    BOOST_CHECK_SMALL(xtds[i], CALCULATION_TOLERANCE);
    BOOST_CHECK_CLOSE(distances[i], atds[i], CALCULATION_TOLERANCE);
  }
}
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_calculate_ground_tracks)
{
  auto points3d(calculate_global_Point3ds(LATITUDES, LONGITUDES));
  Arc3d arc(points3d[5], points3d[6]);

  auto DISTANCE(GOLDEN_ANGLE / 10.0);
  std::vector<double> distances{ 0.0, DISTANCE,
                                 2 * DISTANCE, 3 * DISTANCE,
                                 4 * DISTANCE, 5 * DISTANCE,
                                 6 * DISTANCE, 7 * DISTANCE,
                                 8 * DISTANCE, 9 * DISTANCE };

  auto positions(calculate_positions(arc, distances));
  auto ground_tracks(calculate_azimuths(arc, positions));

  std::vector<double> TRACKS { 2.51327412, 2.54167117,
                               2.56257496, 2.5769034,
                               2.58526799, 2.58801829,
                               2.58526799, 2.5769034,
                               2.56257496, 2.54167117 };

  BOOST_CHECK_EQUAL(ground_tracks.size(), distances.size());

  for (auto i(0u); i < distances.size(); ++i)
    BOOST_CHECK_CLOSE(TRACKS[i], ground_tracks[i], CALCULATION_TOLERANCE);
}
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_calculate_closest_distances)
{
  auto points3d(calculate_global_Point3ds(LATITUDES, LONGITUDES));
  auto arcs3d(calculate_Arc3ds(points3d));

  auto distances(calculate_closest_distances(arcs3d, points3d[5]));
  BOOST_CHECK_EQUAL(distances.size(), arcs3d.size());

  std::vector<double> CLOSEST_DISTANCES { 1.01722197, 1.10714872,
                                          2.03444394, 1.10714872,
                                          0.0,        0.0,
                                          1.10714872, 2.03444394,
                                          2.03444394, 1.10714872,
                                          1.10714872 };

  for (auto i(0u); i < distances.size(); ++i)
    if (CLOSEST_DISTANCES[i])
      BOOST_CHECK_CLOSE(CLOSEST_DISTANCES[i], distances[i], CALCULATION_TOLERANCE);
    else
      BOOST_CHECK_SMALL(distances[i], CALCULATION_TOLERANCE);
}
//////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE_END()
//////////////////////////////////////////////////////////////////////////////
