//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Via Technology Ltd.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//////////////////////////////////////////////////////////////////////////////
/// @file test_Arc3d.cpp
/// @brief Contains unit tests for the via::sphere Arc3d class.
//////////////////////////////////////////////////////////////////////////////
#include "via/sphere/Arc3d.hpp"
#include <boost/test/unit_test.hpp>
#include <iostream>

using namespace via::sphere;

namespace
{
  Point3d NORTH_POLE (global_Point3d( 90.0, 0.0));
  Point3d SOUTH_POLE (global_Point3d(-90.0, 0.0));

  Point3d AMSTERDAM  (global_Point3d( 52.37, 4.90));
  Point3d PARIS      (global_Point3d( 48.86,  2.35));

  Point3d LONDON     (global_Point3d( 51.48, -0.48));
  Point3d NEW_YORK   (global_Point3d( 40.64, -73.76));

  Point3d HONOLULU   (global_Point3d( 21.31, -157.86));
  Point3d TOKYO      (global_Point3d( 35.69, 139.69));

  Point3d SAMOA      (global_Point3d(-13.76, -172.10));
  Point3d WELLINGTON (global_Point3d(-41.29, 174.78));

  Point3d LA         (global_Point3d( 34.05, -118.24));
  Point3d SINGAPORE  (global_Point3d(  1.35, 103.82));

  const auto RAD_TO_MIN(60.0 * RAD_TO_DEG);

  const auto CALCULATION_TOLERANCE(0.00000007);

  const auto MINUS_HALF_CIRCLE = -M_PI_2;
}

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_SUITE(TestArc3d)

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_Arc3d_Constructors_extreme)
{
  Point3d point_1(NEW_YORK);

  Arc3d same_points1(point_1, point_1);
  BOOST_CHECK(point_1 == same_points1.a());
  BOOST_CHECK_EQUAL(0.0, same_points1.length());
  BOOST_CHECK(!same_points1);

  Point3d point_2(NORTH_POLE);
  Arc3d same_points2(point_2, point_2);
  BOOST_CHECK(point_2 == same_points2.a());
  BOOST_CHECK_EQUAL(0.0, same_points2.length());
  BOOST_CHECK(!same_points2);

  // Pole to pole
  Point3d point_3(SOUTH_POLE);
  Arc3d pole2pole(point_2, point_3);
  BOOST_CHECK(point_2 == pole2pole.a());
  BOOST_CHECK_EQUAL(M_PI, pole2pole.length());
  BOOST_CHECK(!pole2pole);
}
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_Arc3d_Constructors_1)
{
  // Nearly Pole to pole
  Point3d point_1 (global_Point3d(-89.9, 0.0));
  Point3d point_2 (global_Point3d(89.9, 0.0));

  Point3d point3d_1(point_1);
  Point3d point3d_2(point_2);

  Arc3d arc3d_1(point_1, point_2);
  BOOST_CHECK_EQUAL(0.0, arc3d_1.azimuth());
  BOOST_CHECK_CLOSE(M_PI, arc3d_1.length(), 0.5);
  BOOST_CHECK_EQUAL(0.0, arc3d_1.calculate_azimuth(arc3d_1.b()));
  BOOST_CHECK(arc3d_1);

  BOOST_CHECK_SMALL(distance_radians(point3d_2, arc3d_1.b()), CALCULATION_TOLERANCE);

  Arc3d arc3d_2(point_2, point_1);
  BOOST_CHECK(!(arc3d_2 == arc3d_1));
  BOOST_CHECK(arc3d_2);
}
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_Arc3d_Constructors_2)
{
  const auto DISTANCE_TOLERANCE(0.1f);

  // Nearly around Equator
  Point3d point3d_1(global_Point3d(0.0, -89.9));
  Point3d point3d_2(global_Point3d(0.0, 89.9));

  Arc3d arc3d_1(point3d_1, point3d_2);
  BOOST_CHECK_CLOSE(M_PI_2, arc3d_1.azimuth(),
                    CALCULATION_TOLERANCE);
  BOOST_CHECK_CLOSE(M_PI, arc3d_1.length(), 0.5);
  BOOST_CHECK_CLOSE(M_PI_2, arc3d_1.calculate_azimuth(arc3d_1.b()),
                    CALCULATION_TOLERANCE);
  BOOST_CHECK_SMALL(distance_radians(point3d_2, arc3d_1.b()), 0.05);
  BOOST_CHECK(arc3d_1);

  Point3d point_0 (global_Point3d(0.0, 0.0));
  BOOST_CHECK_EQUAL(0.0, arc3d_1.cross_track_distance(point_0));
  BOOST_CHECK_CLOSE(M_PI_2 -0.001, arc3d_1.along_track_distance(point_0),
                    DISTANCE_TOLERANCE);
  BOOST_CHECK_EQUAL(0.0, arc3d_1.closest_distance(point_0));

  Point3d point_11 (global_Point3d(1.0, 0.0));
  BOOST_CHECK_CLOSE(DEG_TO_RAD, arc3d_1.cross_track_distance(point_11),
                    DISTANCE_TOLERANCE);
  BOOST_CHECK_CLOSE(DEG_TO_RAD, arc3d_1.closest_distance(point_11),
                    DISTANCE_TOLERANCE);

  Point3d point_12 (global_Point3d(-1.0, 0.0));
  BOOST_CHECK_CLOSE(-DEG_TO_RAD, arc3d_1.cross_track_distance(point_12),
                    DISTANCE_TOLERANCE);
  BOOST_CHECK_CLOSE(DEG_TO_RAD, arc3d_1.closest_distance(point_12),
                    DISTANCE_TOLERANCE);

  Point3d point_13 (global_Point3d(0.0, 80.0));
  BOOST_CHECK_CLOSE(170 * DEG_TO_RAD, arc3d_1.along_track_distance(point_13),
                    DISTANCE_TOLERANCE);
  BOOST_CHECK_EQUAL(0.0, arc3d_1.closest_distance(point_13));

  Point3d point_14 (global_Point3d(0.0, 100.0));
  BOOST_CHECK_CLOSE(-170 * DEG_TO_RAD, arc3d_1.along_track_distance(point_14),
                    DISTANCE_TOLERANCE);
  BOOST_CHECK_CLOSE(10.1 * DEG_TO_RAD, arc3d_1.closest_distance(point_14),
                    DISTANCE_TOLERANCE);
}
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_Arc3d_Constructors_3)
{
  const auto DISTANCE_TOLERANCE(0.1f);

  // Around Equator and international date line
  Point3d point3d_1(global_Point3d(0.0, -135.0));
  Point3d point3d_2(global_Point3d(0.0, 135.0));

  Arc3d arc3d_1(point3d_1, point3d_2);
  BOOST_CHECK_CLOSE(-M_PI_2, arc3d_1.azimuth(),
                    CALCULATION_TOLERANCE);
  BOOST_CHECK_CLOSE(M_PI_2, arc3d_1.length(), 0.5);
  BOOST_CHECK_CLOSE(-M_PI_2, arc3d_1.calculate_azimuth(arc3d_1.b()),
                    CALCULATION_TOLERANCE);
  BOOST_CHECK_SMALL(distance_radians(point3d_2, arc3d_1.b()), CALCULATION_TOLERANCE);
  BOOST_CHECK(arc3d_1);

  Point3d point_0 (global_Point3d(0.0, -180.0));

  BOOST_CHECK_EQUAL(0.0, arc3d_1.cross_track_distance(point_0));
  BOOST_CHECK_CLOSE(M_PI_2 / 2, arc3d_1.along_track_distance(point_0),
                    DISTANCE_TOLERANCE);
  BOOST_CHECK_EQUAL(0.0, arc3d_1.closest_distance(point_0));

  BOOST_CHECK_EQUAL(0.0, arc3d_1.start_angle(point_0));
  BOOST_CHECK_CLOSE(-M_PI, arc3d_1.turn_angle(point_0),
                    CALCULATION_TOLERANCE);

  Point3d point_11 (global_Point3d(1.0, -180.0));
  BOOST_CHECK_CLOSE(-DEG_TO_RAD, arc3d_1.cross_track_distance(point_11),
                    DISTANCE_TOLERANCE);
  BOOST_CHECK_CLOSE(M_PI_2 / 2, arc3d_1.along_track_distance(point_11),
                    0.05);
  BOOST_CHECK_CLOSE(DEG_TO_RAD, arc3d_1.closest_distance(point_11),
                    DISTANCE_TOLERANCE);

  BOOST_CHECK_CLOSE(0.024680177341957217, arc3d_1.start_angle(point_11),
                    CALCULATION_TOLERANCE);
  BOOST_CHECK_CLOSE(3.116912476247836, arc3d_1.turn_angle(point_11),
                    CALCULATION_TOLERANCE);

  Point3d point_12 (global_Point3d(-1.0, -180.0));
  BOOST_CHECK_CLOSE(DEG_TO_RAD, arc3d_1.cross_track_distance(point_12),
                    DISTANCE_TOLERANCE);
  BOOST_CHECK_CLOSE(M_PI_2 / 2, arc3d_1.along_track_distance(point_12),
                    0.05);
  BOOST_CHECK_CLOSE(DEG_TO_RAD, arc3d_1.closest_distance(point_12),
                    DISTANCE_TOLERANCE);

  BOOST_CHECK_CLOSE(-0.024680177341957217, arc3d_1.start_angle(point_12),
                    CALCULATION_TOLERANCE);
  BOOST_CHECK_CLOSE(-3.116912476247836, arc3d_1.turn_angle(point_12),
                     CALCULATION_TOLERANCE);

  Point3d point_13 (global_Point3d(0.0, -90.0));
  BOOST_CHECK_CLOSE(-M_PI_2 / 2, arc3d_1.along_track_distance(point_13),
                    DISTANCE_TOLERANCE);
  BOOST_CHECK_CLOSE(M_PI_2 / 2, arc3d_1.closest_distance(point_13),
                    DISTANCE_TOLERANCE);

  BOOST_CHECK_CLOSE(M_PI, arc3d_1.start_angle(point_13),
                    CALCULATION_TOLERANCE);
  BOOST_CHECK_CLOSE(M_PI, arc3d_1.turn_angle(point_13),
                    CALCULATION_TOLERANCE);

  Point3d point_14 (global_Point3d(0.0, 90.0));
  BOOST_CHECK_CLOSE(3 * M_PI_2 / 2, arc3d_1.along_track_distance(point_14),
                    DISTANCE_TOLERANCE);
  BOOST_CHECK_CLOSE(M_PI_2 / 2, arc3d_1.closest_distance(point_14),
                    DISTANCE_TOLERANCE);

  BOOST_CHECK_EQUAL(0.0, arc3d_1.start_angle(point_14));
  BOOST_CHECK_EQUAL(0.0, arc3d_1.turn_angle(point_14));

  Point3d point_15 (global_Point3d(0.0, 0.0));
  BOOST_CHECK_CLOSE(-3 * M_PI_2 / 2, arc3d_1.along_track_distance(point_15),
                    DISTANCE_TOLERANCE);
  BOOST_CHECK_CLOSE(3 * M_PI_2 / 2, arc3d_1.closest_distance(point_15),
                    DISTANCE_TOLERANCE);

  BOOST_CHECK_CLOSE(-M_PI, arc3d_1.start_angle(point_15),
                    CALCULATION_TOLERANCE);
  BOOST_CHECK_EQUAL(0.0, arc3d_1.turn_angle(point_15));
}
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_Arc3d_Constructors_4)
{
  auto DISTANCE(231.86295916314134); // NM

  Point3d point3d_1(PARIS);
  Point3d point3d_2(AMSTERDAM);

  BOOST_CHECK_CLOSE(DISTANCE, RAD_TO_MIN * distance_radians(point3d_1, point3d_2),
                    CALCULATION_TOLERANCE);

  Arc3d arc3d_1(point3d_1, point3d_2);
  BOOST_CHECK_CLOSE(DISTANCE, RAD_TO_MIN * arc3d_1.length(),
                    CALCULATION_TOLERANCE);
  BOOST_CHECK_CLOSE(23.769880075360707,
                    RAD_TO_DEG * arc3d_1.azimuth(),
                    CALCULATION_TOLERANCE);
  BOOST_CHECK_CLOSE(25.741830161771425,
                    RAD_TO_DEG * arc3d_1.calculate_azimuth(arc3d_1.b()),
                    CALCULATION_TOLERANCE);
  BOOST_CHECK(arc3d_1);
}
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_Arc3d_Constructors_5)
{
  auto DISTANCE(2987.6577639567863); // NM

  Point3d point3d_1(NEW_YORK);
  Point3d point3d_2(LONDON);

  BOOST_CHECK_CLOSE(DISTANCE,
                    RAD_TO_MIN * distance_radians(point3d_1, point3d_2),
                    CALCULATION_TOLERANCE);

  Arc3d arc3d_1(point3d_1, point3d_2);
  BOOST_CHECK_CLOSE(DISTANCE, RAD_TO_MIN * arc3d_1.length(),
                    CALCULATION_TOLERANCE);
  BOOST_CHECK_CLOSE(51.350199195327541,
                    RAD_TO_DEG * arc3d_1.azimuth(),
                    CALCULATION_TOLERANCE);
  BOOST_CHECK_CLOSE(107.90664749220014,
                    RAD_TO_DEG * arc3d_1.calculate_azimuth(arc3d_1.b()),
                    CALCULATION_TOLERANCE);
  BOOST_CHECK(arc3d_1);
}
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_Arc3d_Constructors_6)
{
  auto DISTANCE(3348.4082372998673); // NM, across international date line

  Point3d point3d_1(HONOLULU);
  Point3d point3d_2(TOKYO);

  BOOST_CHECK_CLOSE(DISTANCE,
                    RAD_TO_MIN * distance_radians(point3d_1, point3d_2),
                    CALCULATION_TOLERANCE);

  Arc3d arc3d_1(point3d_1, point3d_2);
  BOOST_CHECK_CLOSE(DISTANCE, RAD_TO_MIN * arc3d_1.length(),
                    CALCULATION_TOLERANCE);
  BOOST_CHECK_CLOSE(-60.524948983927693,
                    RAD_TO_DEG * arc3d_1.azimuth(),
                    CALCULATION_TOLERANCE);
  BOOST_CHECK_CLOSE(-93.033552423067746,
                    RAD_TO_DEG * arc3d_1.calculate_azimuth(arc3d_1.b()),
                    CALCULATION_TOLERANCE);
  BOOST_CHECK(arc3d_1);
}
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_Arc3d_Constructors_7)
{
  auto DISTANCE(1788.3252952821401); // NM , across international date line

  Point3d point3d_1(WELLINGTON);
  Point3d point3d_2(SAMOA);

  BOOST_CHECK_CLOSE(DISTANCE,
                    RAD_TO_MIN * distance_radians(point3d_1, point3d_2),
                    CALCULATION_TOLERANCE);

  Arc3d arc3d_1(point3d_1, point3d_2);
  BOOST_CHECK_CLOSE(DISTANCE, RAD_TO_MIN * arc3d_1.length(),
                    CALCULATION_TOLERANCE);
  BOOST_CHECK_CLOSE(26.331574584597909,
                    RAD_TO_DEG * arc3d_1.azimuth(),
                    CALCULATION_TOLERANCE);
  BOOST_CHECK_CLOSE(20.067894274330907,
                    RAD_TO_DEG * arc3d_1.calculate_azimuth(arc3d_1.b()),
                    CALCULATION_TOLERANCE);
  BOOST_CHECK(arc3d_1);
}
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_Arc3d_Constructors_9)
{
  // A pair of points by the Equator at the Prime Meridian
  Point3d point3d_1(global_Point3d(-1.0, -1.0));
  Point3d point3d_2(global_Point3d(0.0, 0.0));
  Arc3d arc3d_1(point3d_1, point3d_2);
  BOOST_CHECK(arc3d_1);

  // A pair of points by the Equator at the International Date Line
  // on the same Great Circle as the points above.
  Point3d point3d_3(global_Point3d(1.0, 179.0));
  Point3d point3d_4(global_Point3d(0.0, 180.0));
  Arc3d arc3d_2(point3d_3, point3d_4);
  BOOST_CHECK(arc3d_2);

  BOOST_CHECK_SMALL(distance_radians(arc3d_1.pole(), arc3d_2.pole()),
                    CALCULATION_TOLERANCE);
  BOOST_CHECK_CLOSE(arc3d_1.length(), arc3d_2.length(), CALCULATION_TOLERANCE);

  Point3d point3d_5(global_Point3d(-1.0, -179.0));
  Arc3d arc3d_3(point3d_4, point3d_5);
  BOOST_CHECK_SMALL(distance_radians(arc3d_1.pole(), arc3d_3.pole()),
                    CALCULATION_TOLERANCE);
  BOOST_CHECK_CLOSE(arc3d_1.length(), arc3d_3.length(), CALCULATION_TOLERANCE);

  Arc3d arc3d_4(point3d_3, point3d_5);
  BOOST_CHECK_SMALL(distance_radians(arc3d_1.pole(), arc3d_4.pole()),
                    CALCULATION_TOLERANCE);
  BOOST_CHECK_CLOSE(2.0 * arc3d_1.length(), arc3d_4.length(), CALCULATION_TOLERANCE);

  BOOST_CHECK_SMALL(arc3d_1.cross_track_distance(point3d_3), CALCULATION_TOLERANCE);
  BOOST_CHECK_SMALL(arc3d_1.cross_track_distance(point3d_4), CALCULATION_TOLERANCE);
  BOOST_CHECK_SMALL(arc3d_1.cross_track_distance(point3d_5), CALCULATION_TOLERANCE);
}
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_Arc3d_python_repr)
{
  Point3d point_0(WELLINGTON);
  Point3d point_1(SAMOA);

  Arc3d arc(point_0, point_1);
  std::string result
  ("Arc3d([[ -0.748263 0.068361 -0.659871],[-0.209941 0.919155 0.333286]],0.520203)");
  BOOST_CHECK_EQUAL(result, arc.python_repr());
}
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_Arc3d_calculate_intersection)
{
  Point3d point_0(HONOLULU);
  Point3d point_1(TOKYO);
  Arc3d arc1(point_0, point_1);

  // Same arcs, invalid intersection
  auto intersection0(calculate_intersection(arc1, arc1));
  BOOST_CHECK(!intersection0);

  Point3d point_2(LA);
  Point3d point_3(SINGAPORE);
  Arc3d arc2(point_2, point_3);

  // Intersecting arcs, valid intersection
  auto intersection1(calculate_intersection(arc1, arc2));
  BOOST_CHECK(intersection1);
  BOOST_CHECK_CLOSE(35.788545169822555, latitude(intersection1), CALCULATION_TOLERANCE);
  BOOST_CHECK_CLOSE(146.65751944087359, longitude(intersection1), CALCULATION_TOLERANCE);

  // Opposing arcs, invalid intersection
  Arc3d arc3(point_1, point_0);
  auto intersection2(calculate_intersection(arc1, arc3));
  BOOST_CHECK(!intersection2);
}
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_Arc3d_calculate_bisector)
{
  Point3d point_0(NEW_YORK);
  Point3d point_1(LONDON);
  Point3d point_2(AMSTERDAM);

  Arc3d arc1(point_0, point_1);
  Arc3d arc2(point_1, point_2);

  auto arc(calculate_bisector(arc1, arc2));
  auto distance_1(distance_radians(arc1.pole(), arc.pole()));
  auto distance_2(distance_radians(arc2.pole(), arc.pole()));
  BOOST_CHECK_CLOSE(distance_1, distance_2, CALCULATION_TOLERANCE);
}
//////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE_END()
//////////////////////////////////////////////////////////////////////////////
