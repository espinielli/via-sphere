//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Via Technology Ltd.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//////////////////////////////////////////////////////////////////////////////
/// @file test_Point3d.cpp
/// @brief Contains unit tests for the via::sphere::Point3d class.
//////////////////////////////////////////////////////////////////////////////
#include "via/sphere/Point3d.hpp"
#include <boost/test/unit_test.hpp>
#include <limits>
#include <iostream>

using namespace via::sphere;

namespace
{
  const auto CALCULATION_TOLERANCE(0.0000003); // 0.000 000 3% tolerance
}

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_SUITE(TestPoint3d)

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_Point3d_zero)
{
  // Test zero values
  Point3d point(0.0, 0.0, 0.0);
  BOOST_CHECK_EQUAL(0.0, point.x());
  BOOST_CHECK_EQUAL(0.0, point.y());
  BOOST_CHECK_EQUAL(0.0, point.z());
}
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_Point3d_lat_longs)
{
  // Test poles
  Point3d point_south(global_Point3d(-90.0, 180.0));
  Point3d point_north(global_Point3d(90.0, -180.0));

  // Test valid
  BOOST_CHECK(point_south);
  BOOST_CHECK(point_north);

  // Test latitudes
  BOOST_CHECK_EQUAL(-90.0, latitude(point_south));
  BOOST_CHECK_EQUAL(90.0, latitude(point_north));

  // Invalid latitudes
  BOOST_CHECK(point_south == global_Point3d(-90.0 * (1.0 + EPSILON), -180.0));
  BOOST_CHECK(point_north == global_Point3d(90.0 * (1.0 + EPSILON), 180.0));

  // Invalid longitudes
  BOOST_CHECK(point_south == global_Point3d(-90.0, 180.0 * EPSILON));
  BOOST_CHECK(point_north == global_Point3d(90.0, -180.0 * EPSILON));

  // Test Greenwich Equator
  Point3d point_0(global_Point3d(0.0, 0.0));
  BOOST_CHECK(point_0);
  BOOST_CHECK_EQUAL(1.0, point_0.x());
  BOOST_CHECK_EQUAL(0.0, point_0.y());
  BOOST_CHECK_EQUAL(0.0, point_0.z());
  BOOST_CHECK_EQUAL(0.0, latitude(point_0));
  BOOST_CHECK_EQUAL(0.0, longitude(point_0));

  // Test IDL Equator
  Point3d point_1(global_Point3d(0.0, 180.0));
  BOOST_CHECK(point_1);
  BOOST_CHECK_EQUAL(-1.0, point_1.x());
  BOOST_CHECK_SMALL(point_1.y(), CALCULATION_TOLERANCE);
  BOOST_CHECK_EQUAL(0.0, point_1.z());
  BOOST_CHECK_EQUAL(0.0, latitude(point_1));
  BOOST_CHECK_EQUAL(180.0, longitude(point_1));

  Point3d point_2(global_Point3d(0.0, -180.0));
  BOOST_CHECK(point_2);
  BOOST_CHECK_EQUAL(-1.0, point_2.x());
  BOOST_CHECK_SMALL(point_2.y(), CALCULATION_TOLERANCE);
  BOOST_CHECK_EQUAL(0.0, point_2.z());
  BOOST_CHECK_EQUAL(0.0, latitude(point_2));
  BOOST_CHECK_EQUAL(-180.0, longitude(point_2));

  Point3d point_3(global_Point3d(0.0, 3.0 * RAD_TO_DEG));
  BOOST_CHECK(point_3);
  BOOST_CHECK_CLOSE(-0.9899924966, point_3.x(), CALCULATION_TOLERANCE);
  BOOST_CHECK_CLOSE(0.14112000805, point_3.y(), CALCULATION_TOLERANCE);
  BOOST_CHECK_EQUAL(0.0, point_3.z());

  Point3d point_4(global_Point3d(0.0, -3.0 * RAD_TO_DEG));
  BOOST_CHECK(point_4);
  BOOST_CHECK_CLOSE(-0.9899924966, point_4.x(), CALCULATION_TOLERANCE);
  BOOST_CHECK_CLOSE(-0.14112000805, point_4.y(), CALCULATION_TOLERANCE);
  BOOST_CHECK_EQUAL(0.0, point_4.z());

  Point3d point_5(global_Point3d(0.0, (M_PI - 16 * EPSILON) * RAD_TO_DEG));
  BOOST_CHECK(point_5);
  BOOST_CHECK_CLOSE(-0.99999999999999989, point_5.x(), CALCULATION_TOLERANCE);
  BOOST_CHECK_SMALL(point_5.y(), CALCULATION_TOLERANCE);
  BOOST_CHECK_EQUAL(0.0, point_5.z());

  Point3d point_6(global_Point3d(0.0, (-M_PI + 16 * EPSILON) * RAD_TO_DEG));
  BOOST_CHECK(point_6);
  BOOST_CHECK_CLOSE(-0.99999999999999989, point_6.x(), CALCULATION_TOLERANCE);
  BOOST_CHECK_SMALL(point_6.y(), CALCULATION_TOLERANCE);
  BOOST_CHECK_EQUAL(0.0, point_6.z());
}
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_Point3d_functions)
{
  // Test Normal constructed values
  Point3d point_1(1.0, 2.0, 3.0);
  BOOST_CHECK(!point_1);
  BOOST_CHECK_EQUAL(1.0, point_1.x());
  BOOST_CHECK_EQUAL(2.0, point_1.y());
  BOOST_CHECK_EQUAL(3.0, point_1.z());

  BOOST_CHECK_EQUAL(14.0, norm(point_1.p()));
  BOOST_CHECK_CLOSE(std::sqrt(14.0), length(point_1.p()), CALCULATION_TOLERANCE);

  Point3d point_2(3.0, 4.0, 5.0);
  BOOST_CHECK(!point_2);
  BOOST_CHECK_EQUAL(3.0, point_2.x());
  BOOST_CHECK_EQUAL(4.0, point_2.y());
  BOOST_CHECK_EQUAL(5.0, point_2.z());

  BOOST_CHECK_EQUAL(50.0, norm(point_2.p()));
  BOOST_CHECK_CLOSE(std::sqrt(50.0), length(point_2.p()), CALCULATION_TOLERANCE);

  // Test normalize
  Point3d point_3(normalize(point_1.p()));
  BOOST_CHECK(point_3);
  BOOST_CHECK_CLOSE(1.0, norm(point_3.p()), CALCULATION_TOLERANCE);
  BOOST_CHECK_CLOSE(1.0, length(point_3.p()), CALCULATION_TOLERANCE);

  Point3d point_4(normalize(point_2.p()));
  BOOST_CHECK(point_4);
  BOOST_CHECK_CLOSE(1.0, norm(point_4.p()), CALCULATION_TOLERANCE);
  BOOST_CHECK_CLOSE(1.0, length(point_4.p()),  CALCULATION_TOLERANCE);
}
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_Point3d_distance_radians_antipodal)
{
  Point3d SOUTH_POLE(global_Point3d(-90.0, 0.0));
  Point3d NORTH_POLE(global_Point3d(90.0, 0.0));

  BOOST_CHECK_EQUAL(0.0, distance_radians(SOUTH_POLE, SOUTH_POLE));
  BOOST_CHECK_EQUAL(0.0, distance_radians(NORTH_POLE, NORTH_POLE));
  BOOST_CHECK_CLOSE(M_PI, distance_radians(SOUTH_POLE, NORTH_POLE),
                    CALCULATION_TOLERANCE);

  Point3d G_EQ(global_Point3d(0.0, 0.0));
  Point3d IDL_EQ(global_Point3d(0.0, 180.0));

  BOOST_CHECK_EQUAL(0.0, distance_radians(G_EQ, G_EQ));
  BOOST_CHECK_EQUAL(0.0, distance_radians(IDL_EQ, IDL_EQ));
  BOOST_CHECK_CLOSE(M_PI, distance_radians(G_EQ, IDL_EQ), CALCULATION_TOLERANCE);
}
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_Point3d_distance_radians_GUR_JA08F)
{
  Point3d point1(global_Point3d(49.437099, -2.602060)); // GUR
  Point3d point2(global_Point3d(49.6900972, -2.351722)); // JA08F

  auto DISTANCE(18.037152021588373);

  BOOST_CHECK_CLOSE(DISTANCE, 60.0 * RAD_TO_DEG * distance_radians(point1, point2),
                    CALCULATION_TOLERANCE);
}
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_Point3d_ostream)
{
  // Test Normal constructed values
  Point3d point_1(1.0, -2.0, 3.0);
  BOOST_CHECK_EQUAL(1.0, point_1.x());
  BOOST_CHECK_EQUAL(-2.0, point_1.y());
  BOOST_CHECK_EQUAL(3.0, point_1.z());

  using namespace via;
  std::ostringstream ss;
  ss << point_1;

  std::string result("{1,-2,3}");
  BOOST_CHECK_EQUAL(result, ss.str());
}
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_CASE(test_Point3d_python_repr)
{
  Point3d point_1(global_Point3d(49.437099, -2.602060)); // GUR

  std::string result("Point3d([ 0.649612 -0.029522 0.759693 ])");
  BOOST_CHECK_EQUAL(result, point_1.python_repr());
}
//////////////////////////////////////////////////////////////////////////////

BOOST_AUTO_TEST_SUITE_END()
//////////////////////////////////////////////////////////////////////////////
