#ifndef VIA_SPHERE_ARC3D_HPP
#define VIA_SPHERE_ARC3D_HPP

#pragma once

//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Via Technology Ltd.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//////////////////////////////////////////////////////////////////////////////
/// @file Arc3d.hpp
/// @brief Contains the via::sphere::Arc3d class.
//////////////////////////////////////////////////////////////////////////////
#include "Point3d.hpp"

namespace via
{
  /// Provides types and functions for spherical vector geometry.
  namespace sphere
  {

    /// An arc of a Great Circle on a unit sphere in ECEF coordinates.
    /// @invariant a_ & pole_ are normalized
    /// @invariant 0.0 <= length_ <= M_PI
    class Arc3d
    {
#ifdef PYBIND11_NUMPY_DTYPE
    public:
#endif
      Point3d a_;      ///< The start point.
      Point3d pole_;   ///< The right hand pole of the Great Circle.
      double  length_; ///< The length of the Great Circle arc in radians.

#ifndef PYBIND11_NUMPY_DTYPE
    public:
#endif

      /// Default Constuctor.
      /// A zero length, zero width Arc at the South Pole.
      constexpr Arc3d() noexcept
        : a_{0.0, 0.0, -1.0}
        , pole_{1.0, 0.0, 0.0}
        , length_(0.0)
      {}

      /// Values Constuctor.
      /// @param a the start point.
      /// @param pole the right hand pole of the Great Circle.
      /// @param length the the length of the Great Circle arc in radians.
      explicit constexpr Arc3d(Point3d const& a, Point3d const& pole, double  length)
        : a_(a)
        , pole_(pole)
        , length_(length)
      {}

      /// Points Constuctor.
      /// @pre Point3d's a and b are normalized.
      /// @pre MIN_LENGTH < distance_radians(a, b) < M_PI - MIN_LENGTH
      /// @param a, b the start and finish points of a Great Circle arc.
      explicit Arc3d(Point3d const& a, Point3d const& b)
        : a_(a)
        , pole_{0.0, 0.0, 0.0}
        , length_(distance_radians(a, b))
      {
        Vector3d c(cross(a.p(), b.p()));
        if (MIN_NORM < norm(c))
          pole_ = Point3d(normalize(c));
      }

      /// Accessor for the start point.
      /// @return a_
      constexpr Point3d const& a() const noexcept
      { return a_; }

      /// Accessor for the pole.
      /// @return pole_
      constexpr Point3d const& pole() const noexcept
      { return pole_; }

      /// Accessor for the length.
      /// @return length_
      constexpr double length() const noexcept
      { return length_; }

      /// The position of a point at a Great Circle distance along the arc.
      /// @param distance the Great Circle distance in radians.
      /// @return The position of the Point3d along the arc.
      Point3d position(double distance) const noexcept
      {
        return Point3d(std::cos(distance) * a_.p()
                     + std::sin(distance) * cross(pole_.p(), a_.p()));
      }

      /// Accessor for the finish point.
      /// @return b_
      Point3d b() const noexcept
      { return position(length_); }

      /// The position of a perpendicuar point at distance from the Arc.
      /// @param p the Point3d on teh arc.
      /// @param distance the Great Circle distance in radians.
      /// @return The position of the Point3d perpendicuar to the arc.
      Point3d perp_position(Point3d const& p, double distance) const noexcept
      {
        return Point3d(std::cos(distance) * p.p()
                     + std::sin(distance) * pole_.p());
      }

      /// The position of a point at angle from the Arc, at arc length.
      /// @param angle the angle in radians, anti-clockwise is negative.
      /// @return The position of the Point3d at angle to the arc.
      Point3d angle_position(double angle) const noexcept
      {
          // A point M_PI_2 from the centre along the arc rotated by angle
          Point3d angle_pos(std::cos(angle) * cross(pole_.p(), a_.p())
                          - std::sin(angle) * pole_.p());

          // Create an Arc3d to angle_pos and calculate position at length_
          Arc3d angle_arc(a_, angle_pos);
          return angle_arc.position(length_);
      }

      /// Calculate the azimuth at a point on the Arc.
      /// @param p the Point3d.
      /// @return the azimuth at p in radians relative to True North.
      double calculate_azimuth(Point3d const& p) const
      {
        static constexpr Vector3d NORTH_POLE{0.0, 0.0, 1.0};

        Vector3d c(cross(p.p(), NORTH_POLE));
        if (norm(c) > SQ_EPSILON)
        {
          Vector3d pole_cross_c(cross(pole_.p(), c));
          double sine_angle(via::sphere::length(pole_cross_c));
          sine_angle = std::copysign(sine_angle, dot(pole_cross_c, p.p()));
          double cosine_angle(dot(pole_.p(), c));
          return std::atan2(sine_angle, cosine_angle);
        }
        else // point is close to the North or South pole
          return (dot(p.p(), NORTH_POLE) < 0.0) ? 0.0 : M_PI;
      }

      /// Accessor for the azimuth at the start point.
      /// @return the azimuth at a_ in radians.
      double azimuth() const
      { return calculate_azimuth(a_); }

      /// The distance of a point to the left (+ve) or right(-ve) of the
      /// arc's Great Circle in radians.
      /// @param p the Point3d.
      /// @return the distance to the left (+ve) or right(-ve) from the arc's
      /// Great Circle in radians.
      double cross_track_distance(Point3d const& p) const noexcept
      { return std::asin(dot(pole_.p(), p.p())); }

      /// The Great Circle distance of a point along the arc relative to a,
      /// (+ve) ahead of a, (-ve) behind a.
      /// @param p the Point3d.
      /// @return the distance along the Great Circle arc from a in radians:
      /// (+ve) ahead of a, (-ve) behind a.
      double along_track_distance(Point3d const& p) const noexcept
      {
        double atd(distance_radians(a_, p));
        if ((atd > EPSILON) && (atd < (M_PI - EPSILON)))
        {
           double sin2_xtd(dot(pole_.p(), p.p()));
           sin2_xtd *= sin2_xtd;
           double cos2_xtd(1.0 - sin2_xtd);
           if (cos2_xtd > SQ_EPSILON)
           {
             if (sin2_xtd > SQ_EPSILON)
             {
                double cos_xtd(std::sqrt(cos2_xtd));
               // calculate along track distance magnitude
               atd = acos(clamp(std::cos(atd) / cos_xtd, -1.0, 1.0));
            }
             // calculate the sign of the distance: +ve ahead, -ve behind
             double ahead(dot(cross(pole_.p(), a_.p()), p.p()));
             return std::copysign(atd, ahead);
           }
           else // point is close to a pole of the Arc
             return 0.0;
        }
        else // point is close to the Arc start point or it's antipodal point
          return (atd <= EPSILON) ? 0.0 : M_PI;
      }

      /// The closest distance of a point to the Arc3d.
      /// @param p the Point3d.
      /// @return the closest distance to the Arc3d in radians.
      double closest_distance(Point3d const& p) const
      {
        double atd(along_track_distance(p));
        if ((0.0 <= atd) && (atd <= length_))
          return std::abs(cross_track_distance(p));
        else
          return std::min(distance_radians(a(), p),
                          distance_radians(b(), p));
      }

      /// The angle from the start of the Arc3d to a Point3d.
      /// @param p the Point3d.
      /// @return the turn angle in radians, zero if the point is too close
      /// to a_. Note: anti-clockwise is negative.
      double start_angle(Point3d const& p) const
      {
        static constexpr double MAX_LENGTH = M_PI - MIN_LENGTH;

        double d(distance_radians(a_, p));
        if ((MIN_LENGTH < d) && (d < MAX_LENGTH))
        {
          Point3d pole_p(normalize(cross(a_.p(), p.p())));

          double angle(std::acos(clamp(dot(pole_.p(), pole_p.p()), -1.0, 1.0)));
          return std::copysign(angle, -cross_track_distance(p));
        }
        else // Point is too close to or too fsr from a_
          return 0.0;
      }

      /// The turn angle from the end of the Arc3d to a Point3d.
      /// @param p the Point3d.
      /// @return the turn angle in radians, zero if the point is too close
      /// to b_. Note: anti-clockwise is negative.
      double turn_angle(Point3d const& p) const
      {
        Point3d point_b(b());
        double d(distance_radians(point_b, p));
        if (d > MIN_LENGTH)
        {
          Point3d pole_p(normalize(cross(point_b.p(), p.p())));

          double angle(std::acos(clamp(dot(pole_.p(), pole_p.p()), -1.0, 1.0)));
          return std::copysign(angle, -cross_track_distance(p));
        }
        else // Point is too close to point_b
          return 0.0;
      }

      /// Test whether the pole is a valid Point3d.
      /// @return true if the pole is valid.
      bool is_valid() const noexcept
      { return pole_.is_valid(); }

      /// bool operator
      /// @return true if the arc is valid.
      explicit operator bool() const noexcept
      { return is_valid(); }

      /// Equality operator.
      /// @param lhs the left hand side of the expression
      /// @param rhs the right hand side of the expression
      /// @return true if lhs == rhs
      friend bool operator==(Arc3d const& lhs, Arc3d const& rhs) noexcept
      {
        return (lhs.a_ == rhs.a_)
            && (lhs.pole_ == rhs.pole_)
            && (lhs.length_ == rhs.length_);
      }

      /// A Python representation of an Arc3d.
      /// @return a string in Python repr format.
      std::string python_repr() const
      {
          static const std::string ARC3D_START("Arc3d([[ ");
          static const std::string DELIM(" ");
          static const std::string MID_POINT("],[");
          static const std::string END_POINT("]],");
          static const std::string FINISH(")");

          return ARC3D_START + std::to_string(a_.x())
              + DELIM + std::to_string(a_.y())
              + DELIM + std::to_string(a_.z())
              + MID_POINT + std::to_string(pole_.x())
              + DELIM + std::to_string(pole_.y())
              + DELIM + std::to_string(pole_.z())
              + END_POINT + std::to_string(length_)
              + FINISH;
      }

    }; // class Arc3d

    /// Calculate an intersection point of a and b.
    /// @param a, b the Arc3ds.
    /// @return one of the (two) intersection points of a and b, or the
    /// origin if a and b are on the same (or opposing) Great Circles.
    inline Point3d calculate_intersection(Arc3d const& a, Arc3d const& b)
    {
      static constexpr Point3d ORIGIN(0.0, 0.0, 0.0);

      Vector3d c(cross(a.pole().p(), b.pole().p()));
      return (MIN_NORM < norm(c)) ? Point3d(normalize(c)) : ORIGIN;
    }

    /// Calculate the arc bisecting prev_arc and arc.
    /// @param prev_arc, arc the Arc3ds.
    /// @return the arc bisecting prev_arc and arc.
    inline Arc3d calculate_bisector(Arc3d const& prev_arc, Arc3d const& arc)
    {
        Vector3d new_pole(prev_arc.pole().p() + arc.pole().p());
        return Arc3d(arc.a(), Point3d(normalize(new_pole)), arc.length());
    }
  }
}

#endif // VIA_SPHERE_ARC3D_HPP
