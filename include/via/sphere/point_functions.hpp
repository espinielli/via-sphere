#ifndef VIA_SPHERE_POINT_FUNCTIONS_HPP
#define VIA_SPHERE_POINT_FUNCTIONS_HPP

#pragma once

//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Via Technology Ltd.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//////////////////////////////////////////////////////////////////////////////
/// @file point_functions.hpp
/// @brief Contains functions for vectors of Point3ds.
//////////////////////////////////////////////////////////////////////////////
#include "Point3d.hpp"
#include <vector>
#include <algorithm>

namespace via
{
  /// Provides types and functions for spherical vector geometry.
  namespace sphere
  {
    /// Calculate a vector of Point3ds from vectors of latitude and longitude.
    /// @pre !latitudes.empty()
    /// @pre latitudes.size() == longitudes.size()
    /// @param latitudes the latitudes in degrees.
    /// @param longitudes the longitudes in degrees.
    /// @return the Point3ds at latitudes and longitudes.
    inline std::vector<Point3d> calculate_global_Point3ds
                                       (std::vector<double> const& latitudes,
                                        std::vector<double> const& longitudes)
    {
      std::vector<Point3d> points(latitudes.size());

      std::transform(latitudes.cbegin(), latitudes.cend(),
                     longitudes.cbegin(), points.begin(),
                     [](double lat, double lon)
                     { return global_Point3d(lat, lon); });

      return points;
    }

    /// Calculate the latitudes of a vector of Point3ds.
    /// @pre !points.empty()
    /// @param points the vector of Point3ds
    /// @return latitudes of the Point3ds in degrees, North positive.
    inline std::vector<double> calculate_latitudes
                                          (std::vector<Point3d> const& points)
    {
      std::vector<double> values(points.size());

      std::transform(points.cbegin(), points.cend(), values.begin(),
                     [](Point3d const& p) { return latitude(p); });

      return values;
    }

    /// Calculate the longitudes of a vector of Point3ds.
    /// @pre !points.empty()
    /// @param points the vector of Point3ds
    /// @return longitudes of the Point3ds in degrees, East positive.
    inline std::vector<double> calculate_longitudes
                                          (std::vector<Point3d> const& points)
    {
      std::vector<double> values(points.size());

      std::transform(points.cbegin(), points.cend(), values.begin(),
                     [](Point3d const& p) { return longitude(p); });

      return values;
    }

    /// Calculate the Great Circle lengths of the legs between adjacent Point3ds.
    /// @pre points.size() >= 2
    /// @post values.size() == points.size()
    /// @param points the vector of Point3ds
    /// @return leg_lengths of the Point3ds in radians.
    inline std::vector<double> calculate_leg_lengths
                                          (std::vector<Point3d> const& points)
    {
      std::vector<double> values(points.size());
      values[0] = 0.0;

      std::transform(points.cbegin(), points.cend() -1,
                     points.cbegin() +1, values.begin() +1,
                     [](Point3d const& a, Point3d const& b)
                     { return distance_radians(a, b); });

      return values;
    }

    /// Calculate Great Circle distances between a vector of Point3ds and a
    /// Point3d.
    /// @pre !points.empty()
    /// @param points the vector of Point3ds
    /// @param ref_point the reference Point3d to measure distance from.
    /// @return distances of the Point3ds from ref_point in radians.
    inline std::vector<double> calculate_distances
                (std::vector<Point3d> const& points, Point3d const& ref_point)
    {
      std::vector<double> values(points.size());

      std::transform(points.cbegin(), points.cend(), values.begin(),
                     [ref_point](Point3d const& p)
                     { return distance_radians(ref_point, p); });

      return values;
    }

  }
}

#endif // VIA_SPHERE_POINT_FUNCTIONS_HPP
