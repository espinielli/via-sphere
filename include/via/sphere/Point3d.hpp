#ifndef VIA_SPHERE_POINT3D_HPP
#define VIA_SPHERE_POINT3D_HPP

#pragma once

//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Via Technology Ltd.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//////////////////////////////////////////////////////////////////////////////
/// @file Point3d.hpp
/// @brief Contains the via::sphere::Point3d class.
//////////////////////////////////////////////////////////////////////////////
///@mainpage via-sphere: A C++ Spherical Vector Geometry library.
///
/// The library provides classes functions for calculating distances and angles
/// between points and Great Circle arcs on the surface of a sphere.
///
/// It also provides functions to transform positions in latitude/longitude
/// coordinates into points and to calculate latitude/longitude from points.
///
/// @section points_sec Points
///
/// A via::sphere::Point3d is a via::sphere::Vector3d on the surface of a unit
/// sphere in standard Earth Centred, Earth Fixed (ECEF) coordinates.
///
/// The function via::sphere::global_Point3d creates a via::sphere::Point3d
/// from a latitude/longitude.
///
/// The functions: via::sphere::latitude and via::sphere::longitude calculate
/// the latitude and longitude of a via::sphere::global_Point3d.
///
/// The point_functions.hpp file contains functions for vectors of Point3ds.
///
/// @section arcs_sec Arcs
///
/// An via::sphere::Arc3d is the arc of a Great Circle on the surface of the
/// unit sphere.
///
/// The via::sphere::Arc3d class contains methods to calculate: distances,
/// angles and positions of Point3ds relative to an Arc3d.
///
/// The arc_functions.hpp file contains functions for vectors of Point3ds and
/// Arc3ds.
///
//////////////////////////////////////////////////////////////////////////////
#include "Vector3d.hpp"
#include <functional>
#include <limits>
#include <string>

#ifndef M_PI // M_PI is not part of the C or C++ standards: _USE_MATH_DEFINES
constexpr double M_PI   = 3.14159265358979323846;
constexpr double M_PI_2 = 1.57079632679489661923;
#endif

namespace via
{
#if __cplusplus < 201703L
  template<typename T>
  constexpr T clamp(T value, T minimum, T maximum)
  { return std::max(minimum, std::min(maximum, value)); }
#else
  template<typename T>
  constexpr T clamp(T value, T minimum, T maximum)
  { return std::clamp<T>(value, minimum, maximum); }
#endif

  /// Provides types and functions for spherical vector geometry.
  namespace sphere
  {
    /// Factor to convert degrees to radians.
    constexpr double DEG_TO_RAD = M_PI / 180.0;
    /// Factor to convert radians to degrees.
    constexpr double RAD_TO_DEG = 180.0 / M_PI;

    /// The smallest value that can reasonably be measured.
    static constexpr double EPSILON = std::numeric_limits<double>::epsilon();

    /// The square of the smallest value that can reasonably be measured.
    static constexpr double SQ_EPSILON = EPSILON * EPSILON;

    /// The minimum length of a vector to normalize.
    static constexpr double MIN_LENGTH = 16384 * EPSILON;

    /// The square of the vector minimum length.
    static constexpr double MIN_NORM = MIN_LENGTH * MIN_LENGTH;

    /// A 3D vector representing a point on a unit sphere.
    /// The x, y and z coordinates are in the standard (WGS84) Earth Centred,
    /// Earth Fixed (ECEF) configuration.
    /// @invariant x^2 + y^2 + z^2 == 1.0.
    class Point3d
    {
#ifdef PYBIND11_NUMPY_DTYPE
    public:
#endif

      /// The coordinates in a 3 element array.
      Vector3d p_;

#ifndef PYBIND11_NUMPY_DTYPE
    public:
#endif

      /// Default Constuctor.
      /// A point at True North.
      constexpr explicit Point3d() noexcept
        : p_{ 0.0, 0.0, 1.0 }
      {}

      /// Vector3d Constuctor.
      /// @pre length(v) == 1.0
      /// @param v the Vector3d.
      constexpr explicit Point3d(Vector3d const& v) noexcept
        : p_(v)
      {}

      /// Coordinate Constuctor.
      /// @pre x^2 + y^2 + z^2 == 1.0.
      /// @param x, y, z the x, y and z coordinates.
      constexpr explicit Point3d(double x, double y, double z) noexcept
        : p_{ x, y, z}
      {}

      /// Accessor for the coordinates as a Vector3d.
      constexpr Vector3d p() const noexcept
      { return p_; }

      /// Accessor for the x coordinate.
      /// On the plane of the Equator: positive toward the Greenwich Meridian,
      /// negative toward the International Date Line.
      constexpr double x() const noexcept
      { return p_[0]; }

      /// Accessor for the y coordinate.
      /// On the plane of the Equator: East is positive, West is negative.
      constexpr double y() const noexcept
      { return p_[1]; }

      /// Accessor for the z coordinate.
      /// On the Polar axis: Northern Hemishpere is positive,
      /// Southern Hemisphere is negative.
      constexpr double z() const noexcept
      { return p_[2]; }

      /// The length of the vector.
      /// @return the length of the vector.
      double abs() const noexcept
      { return length(p_); }

      /// Test whether the point is valid, i.e.: x^2 + y^2 + z^2 == 1.0.
      /// @return true if length is approximately equal to 1.
      bool is_valid() const noexcept
      {
        static constexpr double MIN_POINT_LENGTH(1.0 - EPSILON);
        static constexpr double MAX_POINT_LENGTH(1.0 + EPSILON);

        auto length(abs());
        return (MIN_POINT_LENGTH <= length) && (length <= MAX_POINT_LENGTH);
      }

      /// bool operator
      /// @return true if the point is on the surface of the sphere.
      explicit operator bool() const noexcept
      { return is_valid(); }

      /// unary minus operator
      Point3d operator-() const noexcept
      { return Point3d(-p_); }

      /// Equality operator.
      /// @param lhs the left hand side of the expression
      /// @param rhs the right hand side of the expression
      /// @return true if lhs == rhs
      friend bool operator==(Point3d const& lhs, Point3d const& rhs) noexcept
      { return lhs.p_ == rhs.p_; }

      /// An ostream operator to output a Point3d.
      template<typename CharT, class Traits>
      friend std::basic_ostream<CharT, Traits>&
      operator<< (std::basic_ostream<CharT, Traits>& os, Point3d const& point)
      {
        std::basic_ostringstream<CharT, Traits> ss;
        ss.flags(os.flags());
        ss.imbue(os.getloc());
        ss.precision(os.precision());
        ss << '{' << point.x() << ',' << point.y() << ',' << point.z() << '}';
        return os << ss.str();
      }

      /// A Python representation of a Point3d.
      /// I.e.: Point3d([ x y z ]
      /// @return a string in Python repr format.
      std::string python_repr() const
      {
        static const std::string POINT3D_START("Point3d([ ");
        static const std::string DELIM(" ");
        static const std::string FINISH(" ])");

        return POINT3D_START + std::to_string(x())
                     + DELIM + std::to_string(y())
                     + DELIM + std::to_string(z())
                     + FINISH;
      }
    };

    /// Calculate the Great Circle distance between two Point3ds.
    /// @param a, b the two Point3ds
    /// @return the distance between a and b in radians.
    inline double distance_radians(Point3d const& a, Point3d const& b) noexcept
    { return std::atan2(length(cross(a.p(), b.p())), dot(a.p(), b.p())); }

    /// Function to calculate a Point3d from a latitude and longitude.
    /// Note: Latitudes > 90 are clamped to the North Pole.
    ///       Latitudes < -90 are clamped to the South Pole.
    ///       Longitudes > 180 or < -180 wrap around the International Date Line.
    /// @param latitude the latitude in degrees.
    /// @param longitude the longitude in degrees.
    /// @return Point3d at latitude and longitude.
    inline Point3d global_Point3d(double latitude, double longitude) noexcept
    {
      double lat(DEG_TO_RAD * clamp(latitude, -90.0, 90.0));
      double lon(DEG_TO_RAD * longitude);

      double sin_lat(std::sin(lat));
      double cos_lat(std::sqrt(1.0 - sin_lat * sin_lat));

      double x(cos_lat * std::cos(lon));
      double y(cos_lat * std::sin(lon));
      double z(sin_lat);

      return Point3d(x, y, z);
    }

    /// Calculate the latitude of a Point3d in degrees.
    /// @param p the p Point3d.
    /// @return the latitude of the Point3d in degrees.
    inline double latitude(Point3d const& p) noexcept
    { return RAD_TO_DEG * std::asin(p.z()); }

    /// Calculate the longitude of a Point3d in degrees.
    /// @param p the p Point3d.
    /// @return the longitude of the Point3d in degrees.
    inline double longitude(Point3d const& p) noexcept
    { return RAD_TO_DEG * std::atan2(p.y(), p.x()); }

  }
}

#endif // VIA_SPHERE_POINT3D_HPP
