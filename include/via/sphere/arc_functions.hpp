#ifndef VIA_SPHERE_ARC_FUNCTIONS_HPP
#define VIA_SPHERE_ARC_FUNCTIONS_HPP

#pragma once

//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Via Technology Ltd.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//////////////////////////////////////////////////////////////////////////////
/// @file arc_functions.hpp
/// @brief Contains functions for vectors of Point3ds and Arc3ds.
//////////////////////////////////////////////////////////////////////////////
#include "Arc3d.hpp"
#include <vector>
#include <algorithm>

namespace via
{
  /// Provides types and functions for spherical vector geometry.
  namespace sphere
  {
    /// Calculate a vector of Arc3ds from a vector of Point3ds.
    /// @pre points.size() >= 2
    /// @post arcs.size() == points.size() -1
    /// @param points the vector of Point3ds
    /// @return the Arc3ds between the Point3ds.
    inline std::vector<Arc3d> calculate_Arc3ds
                                          (std::vector<Point3d> const& points)
    {
      std::vector<Arc3d> arcs(points.size() -1);

      std::transform(points.cbegin(), points.cend() -1,
                     points.cbegin() +1, arcs.begin(),
                     [](Point3d const& a, Point3d const& b)
                     { return Arc3d(a, b); });

      return arcs;
    }

    /// Calculate the turn angles between adjacent Arc3ds in radians.
    /// @pre arcs.size() >= 2
    /// @post values.size() == arcs.size() +1
    /// @param arcs the vector of Arc3ds
    /// @return turn angles between adjacent Arc3ds in radians,
    /// clockwise positive, anti-clockwise negative.
    inline std::vector<double> calculate_turn_angles
                                              (std::vector<Arc3d> const& arcs)
    {
      std::vector<double> values(arcs.size() +1);
      values[0] = 0.0;
      values[values.size() -1] = 0.0;

      std::transform(arcs.cbegin(), arcs.cend() -1,
                     arcs.cbegin() +1, values.begin() +1,
                     [](Arc3d const& a, Arc3d const& b)
                     { return a.turn_angle(b.b()); });

      return values;
    }

    /// Calculate Great Circle across track distances between a vector of
    /// Point3ds and an Arc3d.
    /// @pre !points.empty()
    /// @param arc the reference Arc3d to measure distance from.
    /// @param points the vector of Point3ds
    /// @return across track distances of the Point3ds from the arc in radians.
    inline std::vector<double> calculate_xtds
                        (Arc3d const& arc, std::vector<Point3d> const& points)
    {
      std::vector<double> values(points.size());

      std::transform(points.cbegin(), points.cend(), values.begin(),
                     [arc](Point3d const& p)
                     { return arc.cross_track_distance(p); });

      return values;
    }

    /// Calculate Great Circle along track distances between a vector of
    /// Point3ds and the start of an Arc3d.
    /// @pre !points.empty()
    /// @param arc the reference Arc3d to measure distance from.
    /// @param points the vector of Point3ds
    /// @return along track distances of the Point3ds from the start of the arc
    /// in radians.
    inline std::vector<double> calculate_atds
                        (Arc3d const& arc, std::vector<Point3d> const& points)
    {
      std::vector<double> values(points.size());

      std::transform(points.cbegin(), points.cend(), values.begin(),
                     [arc](Point3d const& p)
                     { return arc.along_track_distance(p); });

      return values;
    }

    /// Calculate the vector of Point3ds at distances along an Arc3d.
    /// @pre !distances.empty()
    /// @param arc the reference Arc3d to calculate positions along.
    /// @param distances the vector of distances along the arc in radians.
    /// @return the vector of Point3ds
    inline std::vector<Point3d> calculate_positions
                      (Arc3d const& arc, std::vector<double> const& distances)
    {
      std::vector<Point3d> points(distances.size());

      std::transform(distances.cbegin(), distances.cend(), points.begin(),
                     [arc](double d){ return arc.position(d); });

      return points;
    }

    /// Calculate the azimuths of a vector of Point3ds along an Arc3d.
    /// @pre !points.empty()
    /// @param arc the reference Arc3d to measure azimuth along.
    /// @param points the vector of Point3ds
    /// @return azimuths of the Point3ds along the arc in radians relative to
    /// True North.
    inline std::vector<double> calculate_azimuths
                        (Arc3d const& arc, std::vector<Point3d> const& points)
    {
      std::vector<double> values(points.size());

      std::transform(points.cbegin(), points.cend(), values.begin(),
                     [arc](Point3d const& p)
                     { return arc.calculate_azimuth(p); });

      return values;
    }

    /// Calculate the closest distances of a vector of Arc3ds from a Point3d.
    /// @pre !arcs.empty()
    /// @param arcs the vector of Arc3ds
    /// @param point the reference Point3d to measure distance from.
    /// @return closest distances of the Arc3ds from the Point3d in radians.
    inline std::vector<double> calculate_closest_distances
                        (std::vector<Arc3d> const& arcs, Point3d const& point)
    {
      std::vector<double> values(arcs.size());

      std::transform(arcs.cbegin(), arcs.cend(), values.begin(),
                     [point](Arc3d const& arc)
                     { return arc.closest_distance(point); });

      return values;
    }

  }
}

#endif // VIA_SPHERE_ARC_FUNCTIONS_HPP
