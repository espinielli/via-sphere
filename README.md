# via-sphere: A Spherical Vector Geometry library

A header-only C++ library with Python bindings for calculating distances, 
angles and positions on the surface of a sphere.

## Description

Spherical trigonometry is the branch of geometry performed on the surface of a
sphere.

Menelaus of Alexandria established a basis for spherical trigonometry nearly two thousand years ago.  
Abu al-Bīrūnī and John Napier (among others) provided equations and tables that have been depended
upon for [global navigation](docs/Global_Navigation.md) across deserts and oceans for
many hundreds of years.

Vector geometry is relatively new compared to spherical geometry,
being only a few hundred years old.  
Vector geometry is widely used in computer systems and can also be used to
perform measurements on the surface of a sphere, see:
[n-vector](http://www.navlab.net/nvector/).

[Spherical vector geometry](docs/Spherical_Vector_Geometry.md) uses a combination
of spherical trigonometry and vector geometry to calculate distances, angles and
positions on the surface of a sphere.

## Design

Points on the surface of sphere are represented by 3D vectors with x, y and z
coordinates see Figure 1.

![Spherical Vector Coordinates](docs/images/ECEF_coordinates.png)  
*Figure 1 Spherical Vector Coordinates*

All distances and angles are in radians.  
Actual distances can be calculated by multiplying by the radius of the sphere.

Functions are provided to convert latitude/longitude coordinates to
spherical vectors and back again.  
Latitude and longitude are in degrees.  
Note: when representing points on the Earth's surface, the 3D vector coordinates
are in the standard WGS84 [ECEF](https://en.wikipedia.org/wiki/ECEF)
orientation with the z axis between the North and South poles, see Figure 1.

C++ and Python functions are provided to process [std::vector](https://en.cppreference.com/w/cpp/container/vector)s
and [numpy array](https://docs.scipy.org/doc/numpy-1.14.0/reference/generated/numpy.array.html)s
of points and Great Circle arcs.

For more information, see [spherical vector design](docs/Spherical_Vector_Design.md)

## Use

Download or clone the latest tagged version of `via-sphere` from
[BitBucket](https://bitbucket.org/viaaero/via-sphere/src/master/).

### C++

Since it's a header only library, it just requires the location of the
[include](include) directory to be added to the `INCLUDE_PATH`.

It should work with any C++11 compliant compiler.  
It's been tested with GCC 8.1.1, Apple Clang 8.0.0 and Visual Studio 2017.

### Python

#### Requirements

The Python bindings, use the `pybind11` library which has has specific C++
compiler requriements, see [pybind11](https://github.com/pybind/pybind11).  

In particular, `pybind11` requires Visual Studio 2015 update 3 or newer on a
Windows machine, or clang 5.0.0 or newer on an Apple machine.

#### Installation

From the directory above where `via-sphere` has been downloaded or cloned,
run the following:

    pip install ./via-sphere
	
To verify the installation, run the tests in: [python/tests](python/tests)  
The python bindings have been tested with python 3.6 and 3.7.

#### Importing

Import the software as `via_sphere`, e.g.:

	from via_sphere import Point3d, global_Point3d, distance_radians
	
#### Docker Container

The [Dockerfile](Dockerfile) installs `via-sphere` into a standard
[python](https://hub.docker.com/_/python/) docker base image.  

## License

`via-sphere` is provided under a MIT license that can be found in the
[LICENSE](LICENSE.txt) file.
